LOCAL_PATH:= $(call my-dir)

#################### v4l2capturedisplay #######################################
include $(CLEAR_VARS)

LOCAL_C_INCLUDES += \
	$(LOCAL_PATH) \
	$(LOCAL_PATH)/util \
	external/libdrm \
	external/libdrm/omap \
	external/libdrm/include/drm

LOCAL_SRC_FILES:= \
	v4l2capturedisplay.c \
	util/display-kms.c \
	util/util.c

LOCAL_SHARED_LIBRARIES:= libdrm libdrm_omap libcutils
LOCAL_MODULE:= v4l2capturedisplay
LOCAL_MODULE_TAGS:= eng
include $(BUILD_EXECUTABLE)

#################### dmabuftest ###############################################

include $(CLEAR_VARS)

LOCAL_C_INCLUDES += \
	$(LOCAL_PATH) \
	$(LOCAL_PATH)/util \
	external/libdrm \
	external/libdrm/omap \
	external/libdrm/include/drm

LOCAL_SRC_FILES:= \
	dmabuftest.c \
	util/v4l2.c \
	util/display-kms.c \
	util/util.c

LOCAL_SHARED_LIBRARIES:= libdrm libdrm_omap libcutils
LOCAL_MODULE:= dmabuftest
LOCAL_MODULE_TAGS:= eng
include $(BUILD_EXECUTABLE)

###############################################################################
