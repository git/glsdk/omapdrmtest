/*
 * Copyright (C) 2012 Texas Instruments
 * Author: Ramprasad <x0038811@ti.com>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <libdce.h>
#include <xf86drm.h>
#include <omap_drm.h>
#include <omap_drmif.h>
#include <string.h>
#include "util.h"


int width , height;
char format[10];

struct YuvRgb {
	struct display *disp;
	int width;
	int height;
	int pitch;
	float bpp;
	int num_outBuf;
	FILE *inFile;
};

static void
usage(char *name)
{
	MSG("Usage: %s -s <connector_id>:<mode> INFILE -W width -H height -c colorformat ", name);
	MSG("example: %s -s 32:1280x800 file.yuv -W 176 -H 144 -c nv12 ", name);
	MSG("example: %s --kmscube --connector 36 file.yuv -W 176 -H 144 -c nv12 ", name);
	MSG("example: %s -w 800x480 file.rgb -W 352 -H 288 -c abgr32 ", name);
	MSG("Test of YUVdisplay");
	MSG("");
	MSG("YUVdisplay options:");
	MSG("\t --help: Print this help and exit.");
	MSG("");
	disp_usage();
}

static void
YuvRgb_close(struct YuvRgb *YuvRgb)
{
    if(!YuvRgb) goto bailout;
	/* free output buffers allocated by display */
	disp_free_buffers(YuvRgb->disp,YuvRgb->num_outBuf);

	if (YuvRgb->disp)           disp_close(YuvRgb->disp);
	free(YuvRgb);
bailout:
	return;
}

static struct YuvRgb *
YuvRgb_open(int argc, char** argv)
{
	struct YuvRgb *YuvRgb;
	char *infile = NULL;
	int i;
	char drm_fourcc[10];

	YuvRgb = calloc(1, sizeof(*YuvRgb));
	if (!YuvRgb)
		return NULL;

	MSG("%p: Opening Display..", YuvRgb);
	YuvRgb->disp = disp_open(argc, argv);
	if (!YuvRgb->disp)
		goto usage;

	/* loop thru args, find input file.. */
	for (i = 1; i < argc; i++) {
		int fd;
		if (!argv[i]) {
			continue;
		}
		fd = open(argv[i], 0);
		if (fd > 0) {
			infile = argv[i];
			argv[i] = NULL;
			close(fd);
			break;
		}
	}

	width  = ALIGN2 (width, 4);        /* round up to macroblocks */
	height = ALIGN2 (height, 4);       /* round up to macroblocks */

	YuvRgb->num_outBuf   = 2;
	YuvRgb->width = width;
	YuvRgb->height = height;

	if(!strcmp(format, "nv12")){
		strcpy(drm_fourcc, "NV12");
		YuvRgb->bpp = 1.5;
	}
	else if(!strcmp(format, "yuyv")){
		strcpy(drm_fourcc, "YUYV");
		YuvRgb->bpp = 2;
	}
	else if(!strcmp(format, "uyvy")){
		strcpy(drm_fourcc, "UYVY");
		YuvRgb->bpp = 2;
	}
	else if(!strcmp(format, "rgb24")){
		strcpy(drm_fourcc, "RG24");
		YuvRgb->bpp = 3;
	}
	else if(!strcmp(format, "bgr24")){
		strcpy(drm_fourcc, "RG24");
		YuvRgb->bpp = 3;
	}
	else if(!strcmp(format, "argb32")){
		strcpy(drm_fourcc, "AR24");
		YuvRgb->bpp = 4;
	}
	else if(!strcmp(format, "abgr32")){
		strcpy(drm_fourcc, "AR24");
		YuvRgb->bpp = 4;
	}
	else{
		MSG("Not a valid format\n");
	}
	MSG("DRM FORUCC is %s\n", drm_fourcc);
	if (check_args(argc, argv) || !infile)
		goto usage;

	/* calculate output buffer parameters: */

	YuvRgb->inFile  = fopen(infile, "rb");
	if(YuvRgb->inFile == NULL){
		ERROR("%p: could not open input file", YuvRgb);
		goto fail;
	}


	if (! disp_get_vid_buffers(YuvRgb->disp, YuvRgb->num_outBuf,
				FOURCC_STR(drm_fourcc), width, height)) {
		ERROR("%p: could not allocate buffers", YuvRgb);
		goto fail;
	}

	YuvRgb->pitch = YuvRgb->width * YuvRgb->bpp;

	MSG("width = %d, height = %d format = %s, pitch = %d\n", width,
			 height, format, YuvRgb->pitch);

	disp_get_fb(YuvRgb->disp);

	return YuvRgb;

usage:
	usage(argv[0]);
fail:
	if (YuvRgb)
		YuvRgb_close(YuvRgb);
	return NULL;
}

static int
YuvRgb_process(struct YuvRgb *YuvRgb)
{
	struct buffer *buf;
	int bufSize = YuvRgb->width * YuvRgb->height * YuvRgb->bpp;
	char *dst;
	int bytes;

	while(1){
		buf = disp_get_vid_buffer(YuvRgb->disp);
		if (!buf) {
			ERROR("%p: fail: out of buffers", YuvRgb);
			return -1;
		}
		dst = (char*)omap_bo_map(buf->bo[0]);
		bytes = fread(dst, bufSize, sizeof(char), YuvRgb->inFile);

		if(bytes == 0 )
			break;

		disp_post_vid_buffer(YuvRgb->disp, buf, 0,0, YuvRgb->width,YuvRgb->height);

		disp_put_vid_buffer(YuvRgb->disp, buf);
	   }

	return 0;
}

int
main(int argc, char **argv)
{
	struct YuvRgb *YuvRgb = NULL;
	int i;
	for (i = 1; i < argc; i++){
		if ( !strcmp(argv[i], "--help")) {
			usage(argv[0]);
			exit(0);
		}
		if(!strcmp(argv[i],"-W")){
			argv[i] = NULL;
			width = atoi(argv[i+1]);
			argv[i+1] = NULL;
			i++;
			continue;
		} if(!strcmp(argv[i], "-H")){
			argv[i] = NULL;
			height = atoi(argv[i+1]);
			argv[i+1] = NULL;
			i++;
			continue;
		} if(!strcmp(argv[i], "-c")){
			argv[i] = NULL;
			strcpy(format,argv[i+1]);
			argv[i+1] = NULL;
			i++;
			continue;
		}

	}
	YuvRgb = YuvRgb_open(argc,argv);

	if(YuvRgb)
		YuvRgb_process(YuvRgb);

	if(YuvRgb)
		YuvRgb_close(YuvRgb);

	YuvRgb = NULL;

	return 0;
}
