/*
 *  Copyright (C) 2009-2012 Texas Instruments, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef _OUTPUT_H
#define _OUTPUT_H

#include <stdio.h>

extern "C" {
#include <util.h>
}

struct BufferMeta {
  struct buffer* buffer;
  int fd[4];
  void* map[4];
  BufferMeta() {
    for (size_t i = 0; i < 4; ++i) {
      fd[i] = 0;
      map[i] = NULL;
    }
  }
};

/*!
 * \file output.h
 * \brief Allocation code based on Rob's
 * \author Hervé Fache
 */

class Output {
protected:
  uint32_t display_width_;
  uint32_t display_height_;
  uint32_t image_width_;
  uint32_t image_height_;
  uint32_t buffer_width_;
  uint32_t buffer_height_;
  uint32_t color_;
  uint32_t numBuffers_;
  bool     twoD_;
  FILE*    fd_;
  void* handle_;
  void* buffers_;
public:
  /**
  * @param display_width The width of the display, needs to be supported!
  * @param display_height The height of the display, needs to be supported!
  * @param image_width The width of the raw image
  * @param image_height The height of the raw image
  * @param buffer_width The requested width of the buffer
  * @param buffer_height The requested height of the buffer
  * @param color The FOURCC color space to use.
  * @param numBuffers The number of desired buffers to internally allocate.
  */
  Output(
      uint32_t display_width, uint32_t display_height,
      uint32_t image_width, uint32_t image_height,
      uint32_t buffer_width, uint32_t buffer_height,
      uint32_t color, uint32_t numBuffers, bool twoD, bool save = false):
    display_width_(display_width), display_height_(display_height),
    image_width_(image_width), image_height_(image_height),
    buffer_width_(buffer_width), buffer_height_(buffer_height),
    color_(color), numBuffers_(numBuffers), twoD_(twoD),
    fd_(save ? stdout /* does not matter, just not NULL */ : NULL) {}
  int open();
  int close();
  struct buffer* alloc(BufferMeta* meta);
  int free(BufferMeta* meta);
  int render(BufferMeta* meta);
};

#endif // _OUTPUT_H

