/*
 *  Copyright (C) 2009-2012 Texas Instruments, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <limits.h>

extern "C" {
#include <util.h>
}

#include <config.h>
#include <output.h>

int Output::open() {
  char* argv[10];
  int argc = 0;
  // Prog name
  char prog[] = "omx_cam_test";
  argv[argc++] = prog;
  // 2D
  char t_option[] = "-t";
  char t_option_value[] = "auto";
  if (twoD_) {
    argv[argc++] = t_option;
    argv[argc++] = t_option_value;
  }
  // Single plane buffers
  char one_option[] = "-1";
  if (! twoD_) {
    argv[argc++] = one_option;
  }
  // KMS display size (ignore by X11)
  char s_option[] = "-s";
  char s_option_value[32];
  sprintf(s_option_value, "%d:%dx%d", 6, display_width_, display_height_);
  argv[argc++] = s_option;
  argv[argc++] = s_option_value;
  // X11 window size (ignored by KMS)
  char w_option[] = "-w";
  char w_option_value[32];
  sprintf(w_option_value, "%dx%d", image_width_, image_height_);
  argv[argc++] = w_option;
  argv[argc++] = w_option_value;
  struct display* d = disp_open(argc, argv);
  if (d == NULL) {
    ERROR("Failed to open display");
    return -1;
  }
  handle_ = d;
  struct buffer** b = disp_get_vid_buffers(d, numBuffers_, color_,
    buffer_width_, buffer_height_);
  if (b == NULL) {
    ERROR("Failed to create buffers");
    return -1;
  }
  buffers_ = b;
  // Also save to file
  if (fd_ != NULL) {
    char file_name[NAME_MAX + 1];
    sprintf(file_name, "output_%dx%d_%dHz.raw", image_width_, image_height_, 30);
    fd_ = fopen(file_name, "w");
    if (fd_ == NULL) {
      ERROR("%m opening file %s\n", file_name);
      return -1;
    }
    MSG("Opened file %s\n", file_name);
  }
  return 0;
}

int Output::close() {
  // Can't see what to call for display close
  if (fd_ != NULL) {
    return fclose(fd_);
  }
  return 0;
}

struct buffer* Output::alloc(BufferMeta* meta) {
  struct display* d = static_cast<struct display*>(handle_);
  struct buffer* b = disp_get_vid_buffer(d);
  MSG(" -> buffer=%p w=%d h=%d fourcc=%x", b, b->width, b->height, b->fourcc);
  if (meta) {
    int p;
    for (p = 0; p < b->nbo; ++p) {
      meta->buffer = b;
      meta->fd[p] = omap_bo_dmabuf(b->bo[p]);
      meta->map[p] = omap_bo_map(b->bo[p]);
      MSG(" ---> bo[%d]=%p pitch=%u fd=%d map=%p", p, b->bo[p], b->pitches[p],
        meta->fd[p], meta->map[p]);
    }
  }
  return b;
}

int Output::free(BufferMeta* meta) {
  struct display* d = static_cast<struct display*>(handle_);
  disp_put_vid_buffer(d, meta->buffer);
  return 0;
}

int Output::render(BufferMeta* meta) {
  struct display* d = static_cast<struct display*>(handle_);
  int rc = disp_post_vid_buffer(d, meta->buffer, 0, 0, image_width_, image_height_);
  if (fd_ != NULL) {
    const char* buffer;
    switch (meta->buffer->fourcc) {
      case FOURCC('U','Y','V','Y'):
      case FOURCC('Y','U','Y','V'):
        buffer = static_cast<const char*>(meta->map[0]);
        for (size_t y = 0; y < image_height_; ++y) {
          size_t j = y * buffer_width_;
          fwrite(&buffer[j], 1, image_width_ * 2, fd_);  // bpp = 16
        }
        break;
      case FOURCC('N','V','1','2'):
        buffer = static_cast<const char*>(meta->map[0]);
        for (size_t y = 0; y < image_height_; ++y) {
          size_t j = y * buffer_width_;
          fwrite(&buffer[j], 1, image_width_, fd_);
        }
        if (meta->buffer->nbo == 1) {
          buffer += meta->buffer->width * meta->buffer->height;
          for (size_t y = 0; y < image_height_ / 2; ++y) {
            size_t j = y * buffer_width_;
            fwrite(&buffer[j], 1, image_width_, fd_);
          }
        } else {
          buffer = static_cast<const char*>(meta->map[1]);
          for (size_t y = 0; y < image_height_ / 2; ++y) {
            size_t j = y * buffer_width_;
            fwrite(&buffer[j], 1, image_width_, fd_);
          }
        }
        break;
      default:
        errno = EINVAL;
        return -1;
    }
  }
  return rc;
}
