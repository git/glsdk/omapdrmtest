/* Copyright (C) 2010 Texas Instruments, Inc. All rights reserved. */

#include <autolock.h>
#include <output.h>
#include <OMXVisionCam.h>

#undef VCAM_SET_FORMAT_ROTATION
#define USE_WB_GAIN_PATCH // enable the hardcoded WB gain get/set apis

#ifdef USE_WB_GAIN_PATCH
    #include "OMXVisionCam_WB_patch.h"
#endif

//#define _USE_GAMMA_RESET_HC_ // enable the hard coded reset system for gamma tables

#include "OMXVisionCam_Gamma_Tbl.h"

#define HERE {printf("=======> OMX - %d <=======", __LINE__);fflush(stdout);}

#ifdef USE_WB_GAIN_PATCH
    #define RED 0
    #define GREEN_RED 1
    #define GREEN_BLUE 2
    #define BLUE 3

    #define CALCULATE_WB_GAINS_OFFSET(type,in,out) out = (type *)(in + 4 + in[4] + in[8])
#endif // USE_WB_GAIN_PATCH

#define GAMMA_TABLE_SIZE 1024*sizeof(uint16_t)

#define OMX_CHECK(error, function)  {\
    error = function;\
    if (error != OMX_ErrorNone) {\
        ERROR("OMX Error 0x%08x in "#function" on line # %u", error, __LINE__);\
    }\
}

#define OMX_CONVERT_RETURN_IF_ERROR(error, function) {\
    error = function;\
    if (error != OMX_ErrorNone) {\
        ERROR("OMX Error 0x%08x in "#function" on line # %u", error, __LINE__);\
        return ConvertError(error);\
    }\
}

#define OMX_STRUCT_INIT(str, type, pVersion)  {\
    memset(&str, 0, sizeof(type));\
    str.nSize = sizeof(type);\
    memcpy(&str.nVersion, pVersion, sizeof(OMX_VERSIONTYPE));\
    str.nPortIndex = OMX_ALL;\
}

#define OMX_STRUCT_INIT_PTR(ptr, type, pVersion) {\
    memset(ptr, 0, sizeof(type));\
    ptr->nSize = sizeof(type);\
    memcpy(&ptr->nVersion, pVersion, sizeof(OMX_VERSIONTYPE));\
    ptr->nPortIndex = OMX_ALL;\
}

#define LOOP_PORTS( port, cnt ) \
        for(    cnt = ( VCAM_PORT_ALL == port ? VCAM_PORT_PREVIEW : port ); \
                cnt < ( VCAM_PORT_ALL == port ? VCAM_PORT_MAX : port + 1 ); \
                cnt++ \
            )

void* FocusThreadLauncher(void *arg)
{
    OMXVisionCam *pCam = reinterpret_cast<OMXVisionCam *>(arg);
    pCam->waitForFocus();
    return 0;
}

void* PreemptionThreadLauncher( void *arg )
{
    OMXVisionCam *pCam = reinterpret_cast<OMXVisionCam *>(arg);
    pCam->PreemptionService();
    return 0;
}

void* FrameThreadFunc(void* user) {
  OMXVisionCam* pCam = static_cast<OMXVisionCam*>(user);
  OMX_IN OMX_BUFFERHEADERTYPE* pBuffHeader;
  do {
    if (pCam->mFrameCbData.frames.empty()){
      pthread_mutex_lock(&pCam->mFrameCbData.mutex);
    } else {
      pBuffHeader = pCam->mFrameCbData.frames.front();
      pCam->mFrameCbData.frames.pop_front();
      if (pBuffHeader != NULL) {
        pCam->frameReceivedSrvc(pBuffHeader);
      }
    }
  } while (pBuffHeader != NULL);

  return NULL;
}

static void PrintOMXState(OMX_STATETYPE state)
{
    const char *state_names[] = {
        "OMX_StateInvalid",
        "OMX_StateLoaded",
        "OMX_StateIdle",
        "OMX_StateExecuting",
        "OMX_StatePause",
        "OMX_StateWaitForResources"
    };
    if (state < (sizeof(state_names) / sizeof(const char*))) {
        MSG("OMX-CAMERA is in state %s", state_names[state]);
    }
}

OMX_U32 frameRates[] = {1, 5, 15, 24, 30, 60};


/* Constructor - Open the Shared Library, containing the Camera Adapter and
* gen an instance of the Adapter.
*/
OMXVisionCam::OMXVisionCam()
{
    m_callback = NULL;
    m_focuscallback = NULL;
    pthread_mutex_init(&mFrameBufferLock, NULL);
    pthread_mutex_init(&mUserRequestLock, NULL);
    memset(&mCurGreContext, 0, sizeof(mCurGreContext));
}

int OMXVisionCam::init(void *cookie)
{
    mFlushInProcess = false;
    OMX_ERRORTYPE omxError = OMX_ErrorNone;
    int greError = 0;

#if TIME_PROFILE
    PopulateTimeProfiler();
#endif // TIME_PROFILE

    MSG("OMXVisionCam::OMXVisionCam");
    MSG("OMXVisionCam compiled: %s, %s", __DATE__, __TIME__ );
    mLocalVersion = new OMX_VERSIONTYPE();
    m_callback = NULL;
    m_focuscallback = NULL;

    m_cookie = cookie; // save the cookie value;

    mLocalVersion->s.nVersionMajor = 1;
    mLocalVersion->s.nVersionMinor = 1;
    mLocalVersion->s.nRevision = 0 ;
    mLocalVersion->s.nStep =  0;

    sem_init(&mGreLocalSem, 0, 1); // set the max count
    sem_wait(&mGreLocalSem); // pre-decrement the current count otherwise the first wait will pass early
    sem_init(&mGreFocusSem, 0, 1); // set the max count
    sem_wait(&mGreFocusSem); // pre-decrement the current count otherwise the first wait will pass early

    // Initialize the Vision Core
    MSG("Calling OMX_Init()");
    OMX_CONVERT_RETURN_IF_ERROR(omxError, OMX_Init());

    OMX_CALLBACKTYPE omxCallbacks;
    omxCallbacks.EventHandler    = OMXVisionCam::EventHandler;
    omxCallbacks.EmptyBufferDone = OMXVisionCam::EmptyBufferDone;
    omxCallbacks.FillBufferDone  = OMXVisionCam::FillBufferDone;

    memset( &(mCurGreContext), 0, sizeof( VCAM_ComponentContext ) );

    mCurGreContext.mPortsInUse[VCAM_PORT_ALL] = OMX_ALL;
    mCurGreContext.mPortsInUse[VCAM_PORT_PREVIEW] = VCAM_CAMERA_PORT_VIDEO_OUT_PREVIEW;
    mCurGreContext.mPortsInUse[VCAM_PORT_VIDEO]   = VCAM_CAMERA_PORT_VIDEO_OUT_VIDEO;

    for( int i = VCAM_PORT_PREVIEW; i < VCAM_PORT_MAX; i++ )
    {
        mBuffersInUse[i].mBuffers = NULL;
        mBuffersInUse[i].mNumberBuffers = 0;
        mCurGreContext.mCameraPortParams[i].mColorFormat = OMX_COLOR_FormatYUV420SemiPlanar;
        mCurGreContext.mCameraPortParams[i].mWidth = QVGA_WIDTH;
        mCurGreContext.mCameraPortParams[i].mHeight = QVGA_HEIGHT;
        mCurGreContext.mCameraPortParams[i].mFrameRate = INITIAL_FRAMERATE;
        for(uint32_t buf = 0; buf < VCAM_NUM_BUFFERS; buf++ )
            mCurGreContext.mCameraPortParams[i].mBufferHeader[buf] = NULL;
        mFrameDescriptors[i] = NULL;
    }

    mCurGreContext.mHandleComp = NULL;

    MSG("Calling OMX_GetHandle()");
    OMX_CONVERT_RETURN_IF_ERROR(omxError,OMX_GetHandle(&( mCurGreContext.mHandleComp ),
                                         (OMX_STRING)"OMX.TI.DUCATI1.VIDEO.CAMERA",
                                         this ,
                                         &omxCallbacks));
    GetDucatiVersion();

    OMX_CONVERT_RETURN_IF_ERROR(omxError,OMX_SendCommand(mCurGreContext.mHandleComp,
                                                         OMX_CommandPortDisable,
                                                         OMX_ALL,
                                                         NULL));


// #if defined(BLAZE) || defined(SDP) || defined(BLAZE_TABLET)
    {
        OMX_U32 pr = 4;

        memcpy( &mVisionCamPriority.nVersion, mLocalVersion, sizeof(OMX_VERSIONTYPE));
        mVisionCamPriority.nGroupID = CAMERA_GROUP_ID;
        mVisionCamPriority.nGroupPriority = pr;
        mVisionCamPriority.nSize = sizeof(OMX_PRIORITYMGMTTYPE);

        omxError = OMX_SetParameter(mCurGreContext.mHandleComp, OMX_IndexParamPriorityMgmt ,&mVisionCamPriority);
    }
// #else
//     ERROR("No Priority Management is enabled on this platform!");
// #endif

    pthread_mutex_lock(&mFrameCbData.mutex);
    pthread_create(&mFrameThread, NULL, FrameThreadFunc, this);
    mUseFramePackaging = false;

    m_frameNum = 0;
    mPreemptionState = VCAM_PREEMPT_INACTIVE;
    mFaceDetectionEnabled = false;
    mReturnToExecuting = false;
#ifdef _USE_GAMMA_RESET_HC_
    mGammaResetPolulated = false;
#endif // _USE_GAMMA_RESET_HC_

    return greError;
}

int OMXVisionCam::deinit()
{
    int greError = 0;
    OMX_ERRORTYPE omxError = OMX_ErrorNone;

    // Free the handle for the Camera component
    if (mCurGreContext.mHandleComp)
    {
        // free the handle
        MSG("Calling OMX_FreeHandle(0x%08x)", (unsigned)(mCurGreContext.mHandleComp));
        omxError = OMX_FreeHandle(mCurGreContext.mHandleComp);
        greError = ConvertError(omxError);
        mCurGreContext.mHandleComp = 0;
        if( 0 != greError )
        {
            ERROR("ERROR: error freeing OMX handle.OMX_FreeHandle() returned 0x%x", omxError);
        }

        // Deinitialize the OMX Core
        MSG("Calling OMX_Deinit()");
        omxError = OMX_Deinit();
        greError = ConvertError(omxError);
        if( 0 != greError )
        {
            ERROR("ERROR: error in OMX_Deinit.OMX err: 0x%x", omxError);
        }
    }

    mFrameCbData.frames.push_back(NULL);
    pthread_mutex_unlock(&mFrameCbData.mutex);
    pthread_join(mFrameThread, NULL);

#if TIME_PROFILE
    for(int i = 0; i < VCAM_TIME_TARGET_MAX; i++)
        if( mTimeProfiler[i] )      // dump and clear
            delete mTimeProfiler[i];
#endif

    delete mLocalVersion;

    sem_destroy(&mGreLocalSem);
    sem_destroy(&mGreFocusSem);

    return greError;
}

/* Destructor - free all recources used by Vision Cam and from us */
OMXVisionCam::~OMXVisionCam()
{
    pthread_mutex_destroy(&mFrameBufferLock);
    pthread_mutex_destroy(&mUserRequestLock);
    MSG("OMX Vision Cam is destroyed!");
}

/**
* This is used to get esier the state of component.
*/
inline OMX_STATETYPE OMXVisionCam::getComponentState()
{
    OMX_STATETYPE state = OMX_StateInvalid;

    if( mCurGreContext.mHandleComp )
    {
        OMX_GetState( mCurGreContext.mHandleComp, &state );
    }
    PrintOMXState(state);
    return state;
}

/**
* Initialisation of OMX_PARAM_PORTDEFINITIONTYPE
* needed for various configurations
*/
inline OMX_ERRORTYPE OMXVisionCam::initPortCheck( OMX_PARAM_PORTDEFINITIONTYPE * portCheck , OMX_U32 portIndex )
{
    OMX_ERRORTYPE omxError = OMX_ErrorNone;
    OMX_STRUCT_INIT_PTR(portCheck, OMX_PARAM_PORTDEFINITIONTYPE, mLocalVersion)

    if( VCAM_PORT_ALL < portIndex && VCAM_PORT_MAX > portIndex )
    {
        portCheck->nPortIndex = mCurGreContext.mPortsInUse[portIndex];
        omxError = OMX_GetParameter(mCurGreContext.mHandleComp,
                                    OMX_IndexParamPortDefinition,
                                    portCheck);
        MSG("PORT CHECK[%u], E:%d P:%d B#:%u %ux%u C:%d",
                  (uint32_t)portCheck->nPortIndex,
                  portCheck->bEnabled,
                  portCheck->bPopulated,
                  (uint32_t)portCheck->nBufferCountActual,
                  (uint32_t)portCheck->format.video.nFrameWidth,
                  (uint32_t)portCheck->format.video.nFrameHeight,
                  portCheck->format.video.eColorFormat);

    }
    else
        omxError = OMX_ErrorBadParameter;

    if( omxError != OMX_ErrorNone )
    {
        ERROR("OMX_GetParameter - 0x%x in initPortCheck(%lu)",
                  omxError, mCurGreContext.mPortsInUse[portIndex]);
    }
    return omxError;
}
/**
   @brief Method to convert from OMX_ERRORTYPE to GesturError_e
   @param error Any of the standard OMX error codes defined in the OpenMAX 1.x Specification.
   @return int
 */
int OMXVisionCam::ConvertError(OMX_ERRORTYPE error)
{
    int status = 0;
    switch(error)
    {
        case OMX_ErrorNone:
            status = 0;
            break;
        case OMX_ErrorBadParameter:
            status = -EINVAL;
            break;
        case OMX_ErrorIncorrectStateOperation:
            status = -EBADE;
            break;
        case OMX_ErrorHardware:
            status = -EIO;
            break;
        default:
            status = -ESTALE; /* Something stupid */
            break;
    }
    if (error != OMX_ErrorNone) {
        ERROR("Converting OMX Error 0x%08x to int %d", error, status);
    }
    return status;
}

int OMXVisionCam::getLutValue( int searchVal, ValueTypeOrigin origin, const int lut[][2], int lutSize)
{
    int idxToGet = (origin == VCAM_VALUE_TYPE ? OMX_VALUE_TYPE : VCAM_VALUE_TYPE );
    for( int i = 0; i < lutSize; i++ )
    {
        if( lut[i][origin] == searchVal )
            return lut[i][idxToGet];
    }

    return -EINVAL;
}

void OMXVisionCam::GetDucatiVersion()
{
    if( mCurGreContext.mHandleComp )
    {
        OMX_VERSIONTYPE compVersion;
        OMX_VERSIONTYPE specVersion;
        char compName[128];
        OMX_UUIDTYPE compUUID[128];
        OMX_ERRORTYPE omxError = OMX_ErrorNone;
        MSG("Querying Component Version!");
        omxError = OMX_GetComponentVersion(mCurGreContext.mHandleComp,
                                           compName,
                                           &compVersion,
                                           &specVersion,
                                           compUUID);
        if (omxError == OMX_ErrorNone)
        {
            MSG("\tComponent Name:    [%s]",   compName);
            MSG("\tComponent Version: [%u]",   (unsigned int)compVersion.nVersion);
            MSG("\tSpec Version:      [%u]",   (unsigned int)specVersion.nVersion);
            MSG("\tComponent UUID:    [%s]", (char*)compUUID);
        }
    }
}

int OMXVisionCam::transitToState(OMX_STATETYPE targetState, serviceFunc transitionService, void * data )
{
    int greError = 0;
    OMX_ERRORTYPE omxError = OMX_ErrorNone;
    sem_t sem; // this semaphore does not need to be global

    if( OMX_StateInvalid == targetState )
        return -EINVAL;

    sem_init(&sem, 0, 1);
    sem_wait(&sem); // predecrement so that the later wait will actually block

    omxError = RegisterForEvent( mCurGreContext.mHandleComp,
                                    OMX_EventCmdComplete,
                                    OMX_CommandStateSet,
                                    targetState,
                                    &sem,
                                    -1 // Infinite timeout
                                );

    if( OMX_ErrorNone == omxError )
    {
        omxError = OMX_SendCommand( mCurGreContext.mHandleComp ,
                                                   OMX_CommandStateSet,
                                                   targetState,
                                                   NULL );
        switch( omxError )
        {
            case OMX_ErrorNone:
            {
                if( transitionService )
                {
                    greError = transitionService( this, data );
                }
                break;
            }

            case OMX_ErrorIncorrectStateTransition:
            {
                greError = -EINVAL;
                break;
            }

            case OMX_ErrorInsufficientResources:
            {
                if( OMX_StateIdle == targetState )
                {
                    mPreemptionState = VCAM_PREEMPT_WAIT_TO_START;
                    greError = PreemptionService();
                }
                break;
            }

            case OMX_ErrorSameState:
                break;

            default:
                greError = ConvertError(omxError);
                break;
        }

        if( greError != 0 || OMX_ErrorSameState == omxError )
        {
            // unregister the event
            EventHandler( mCurGreContext.mHandleComp,
                          this,
                          (OMX_EVENTTYPE)OMX_EventCmdComplete,
                          (OMX_U32)OMX_CommandStateSet,
                          (OMX_U32)targetState,
                          NULL
                        );
        }
        else //( 0 == greError )
        {
            MSG("Waiting for state transition. State requested: %d", (int)targetState);
            sem_wait(&sem);
            MSG("Camera is now in state %d", (int)targetState);
        }
    }

    sem_destroy(&sem);

    return greError;
}

int OMXVisionCam::portEnableDisable( OMX_COMMANDTYPE enCmd, serviceFunc enablementService, VisionCamPort_e port )
{
    int greError = 0;
    OMX_ERRORTYPE omxError = OMX_ErrorNone;

    OMX_PARAM_PORTDEFINITIONTYPE portCheck;
    VCAM_PortParameters * portData = NULL;

    mCurGreContext.mCameraPortParams[VCAM_PORT_ALL].mIsActive = true;

    int32_t p;
    LOOP_PORTS( port , p )
    {
        initPortCheck(&portCheck, p);
        portData = &mCurGreContext.mCameraPortParams[p];

        // check to see if the port is already enabled/disabled and that we wanted that state
        if( (OMX_TRUE == portCheck.bEnabled) && (enCmd == OMX_CommandPortEnable) )
        {
            portData->mIsActive = true;
            continue;
        }

        if( (OMX_FALSE == portCheck.bEnabled) && (enCmd == OMX_CommandPortDisable) )
        {
            portData->mIsActive = false;
            continue;
        }

        // we wanted to transition from one state to the other

        // ports with no buffers can't be enabled or disabled.
        if( portData->mNumBufs == 0 )
            continue;

        // we have buffers, so enable/disable the port!
        MSG("Registering for port enable/disable event");
        omxError = RegisterForEvent(    mCurGreContext.mHandleComp,
                                        OMX_EventCmdComplete,
                                        enCmd,
                                        mCurGreContext.mPortsInUse[p],
                                        &mGreLocalSem,
                                        -1 /*Infinite timeout */
                                    );

        if( OMX_ErrorNone == omxError )
        {
            omxError = OMX_SendCommand( mCurGreContext.mHandleComp,
                                        enCmd,
                                        mCurGreContext.mPortsInUse[p],
                                        NULL
                                        );
        }

        if( enablementService && OMX_ErrorNone == omxError )
        {
            greError = enablementService( this, (void*)(&p) );
        }

        if( OMX_ErrorNone == omxError && 0 == greError )
        {
            sem_wait(&mGreLocalSem);
        }
        else
        {
            // unregister event
            EventHandler( mCurGreContext.mHandleComp,
                        this,
                        (OMX_EVENTTYPE)OMX_EventCmdComplete,
                        (OMX_U32)enCmd,
                        (OMX_U32)mCurGreContext.mPortsInUse[p],
                        NULL
                        );
        }

        initPortCheck(&portCheck, p);

        if( OMX_TRUE == portCheck.bEnabled )
        {
            portData->mIsActive = true;
            mFramePackage.mExpectedFrames[p] = true;
        }
        else
        {
            portData->mIsActive = false;
            mFramePackage.mExpectedFrames[p] = false;
        }
    }

    mCurGreContext.mCameraPortParams[VCAM_PORT_ALL].mIsActive = true;
    int32_t p2;
    LOOP_PORTS(VCAM_PORT_ALL, p2)
    {
        if( mCurGreContext.mCameraPortParams[p2].mIsActive == false )
        {
            mCurGreContext.mCameraPortParams[VCAM_PORT_ALL].mIsActive = false;
            break;
        }
    }

    return greError;
}

int OMXVisionCam::fillPortBuffers( VisionCamPort_e port )
{
    int greError = 0;
    OMX_ERRORTYPE omxError = OMX_ErrorNone;
    VCAM_PortParameters *portData = NULL;

    portData = &mCurGreContext.mCameraPortParams[port];
    if (portData->mIsActive == true) {

        for( int index = 0; index < portData->mNumBufs; index++ )
        {
            if( portData && portData->mBufferHeader[index] )
            {
                MSG("FILL BUFF HDR[%d]:%p PORT:%u", index, portData->mBufferHeader[index], (uint32_t)mCurGreContext.mPortsInUse[port]);
                omxError = OMX_FillThisBuffer(mCurGreContext.mHandleComp,
                                              (OMX_BUFFERHEADERTYPE*)( portData->mBufferHeader[index]));
                if (omxError != OMX_ErrorNone)
                {
                    ERROR("OMX_FillThisBuffer() returned error  0x%x", omxError );
                    greError = ConvertError(omxError);
                    break;
                }
            }
            else
            {
                greError = -ENOMEM;
            }
        }
    }
    else
    {
        greError = -EBADE;
    }
    if (omxError != OMX_ErrorNone)
    {
        greError = ConvertError(omxError);
    }

    return greError;
}

int OMXVisionCam::freePortBuffers( VisionCamPort_e port )
{
    int greError = 0;
    OMX_ERRORTYPE omxError = OMX_ErrorNone;
    VCAM_PortParameters *portData = NULL;

    AutoLock lock(&mFrameBufferLock);

    int32_t p;
    LOOP_PORTS( port , p )
    {
        portData = &mCurGreContext.mCameraPortParams[p];
        for( int buff = 0; buff < portData->mNumBufs; buff++ )
        {
            omxError = OMX_FreeBuffer( mCurGreContext.mHandleComp,
                                        mCurGreContext.mPortsInUse[p],
                                        portData->mBufferHeader[buff]
                                      );
            portData->mBufferHeader[buff] = NULL;
            if (omxError != OMX_ErrorNone)
            {
                ERROR("OMX_FreeBuffer() returned error  0x%x", omxError );
                greError = ConvertError(omxError);
                break;
            }
        }
    }

    return greError;
}

int OMXVisionCam::populatePort( VisionCamPort_e port )
{
    int greError = 0;
    OMX_ERRORTYPE omxError = OMX_ErrorNone;
    VCAM_PortParameters * data = NULL;

    int32_t p;
    LOOP_PORTS( port , p )
    {
        OMX_TI_PARAM_USEBUFFERDESCRIPTOR desc;
        OMX_STRUCT_INIT(desc, OMX_TI_PARAM_USEBUFFERDESCRIPTOR, mLocalVersion);
        // No idea
        desc.nPortIndex = mCurGreContext.mPortsInUse[p];
        // For 2D buffer
        desc.bEnabled = OMX_FALSE;
        omxError = OMX_SetParameter(mCurGreContext.mHandleComp, (OMX_INDEXTYPE) OMX_TI_IndexUseDmaBuffers, &desc);
        MSG("Configuring port %u for DMAbuf handles (err=0x%08x)", p, omxError);
        data = &mCurGreContext.mCameraPortParams[p];

        for( int indBuff = 0; indBuff < data->mNumBufs; indBuff++ )
        {
            OMX_BUFFERHEADERTYPE *pBufferHdr;
            OMX_U8 *buffer = NULL;

            // Pass array of file descriptors
            buffer = (OMX_U8*)mBuffersInUse[p].mBuffers[indBuff].fd;
            MSG("VCAM: Using FDs %d and %d", ((int*) buffer)[0], ((int*) buffer)[1]);
            omxError = OMX_UseBuffer( mCurGreContext.mHandleComp,
                                        &pBufferHdr,
                                        mCurGreContext.mPortsInUse[p],
                                        0,
                                        data->mBufSize,
                                        buffer );

            if( OMX_ErrorNone != omxError )
            {
                ERROR("VCAM: ERROR: OMX_UseBuffer() returned 0x%x ( %d )", omxError, omxError);
                greError = ConvertError(omxError);
                break;
            }

            pBufferHdr->pAppPrivate = (OMX_PTR)&mBuffersInUse[p].mBuffers[indBuff];
            MSG("pAppPrivate=%p mBuffers[indBuff]=%p", pBufferHdr->pAppPrivate, &mBuffersInUse[p].mBuffers[indBuff]);
            pBufferHdr->nSize = sizeof(OMX_BUFFERHEADERTYPE);
            memcpy( &(pBufferHdr->nVersion), mLocalVersion, sizeof( OMX_VERSIONTYPE ) );

            data->mBufferHeader[indBuff] = pBufferHdr;

            if( NULL == mFrameDescriptors[p][indBuff]->mFrameBuff )
                    mFrameDescriptors[p][indBuff]->mFrameBuff = pBufferHdr->pAppPrivate;
        }
    }

    return greError;
}

int OMXVisionCam::useBuffers(BufferMeta* meta, uint32_t numImages, VisionCamPort_e port )
{
    int greError = 0;

    if ( numImages == 0  || port <= VCAM_PORT_ALL || port >= VCAM_PORT_MAX)
    {
        ERROR("Invalid parameters to useBuffers()");
        return -EINVAL;
    }

    // alloc the array for frame descriptors
    if( !mFrameDescriptors[port] )
    {
        mFrameDescriptors[port] = new VisionCamFrame* [numImages];
        if( NULL == mFrameDescriptors[port] )
        {
            greError = -ENOMEM;
        }

        for( uint32_t i = 0; i < numImages && 0 == greError; i++ )
        {
            mFrameDescriptors[port][i] = new VisionCamFrame();

            if( mFrameDescriptors[port][i] )
            {
                mFrameDescriptors[port][i]->clear();
            }
            else
            {
                while( i )
                    delete mFrameDescriptors[port][--i];

                delete [] mFrameDescriptors[port];
                mFrameDescriptors[port] = NULL;

                greError = -ENOMEM;
            }
        }
    }

    if( 0 == greError )
    {
        VCAM_PortParameters * data = &mCurGreContext.mCameraPortParams[port];

        mBuffersInUse[port].mBuffers = meta;
        mBuffersInUse[port].mNumberBuffers = (unsigned)numImages;

        data->mNumBufs = mBuffersInUse[port].mNumberBuffers;
        data->mStride = mBuffersInUse[port].mBuffers[0].buffer->pitches[0];

        if( OMX_StateIdle != getComponentState() )
        {
            greError = transitToState( OMX_StateIdle );
        }
    }

    return greError;
}

int OMXVisionCam::flushBuffers( VisionCamPort_e port)
{
    int greError = 0;
    OMX_ERRORTYPE omxError = OMX_ErrorNone;

    if( OMX_StateExecuting != getComponentState() )
    {
        return greError;
    }

    int32_t p;
    LOOP_PORTS( port , p )
    {
        sem_t sem;
        sem_init(&sem, 0, 1);
        sem_wait(&sem); // predecrement so that the next wait won't fire ahead of time.
        omxError = RegisterForEvent(mCurGreContext.mHandleComp,
                                    OMX_EventCmdComplete,
                                    OMX_CommandFlush,
                                    mCurGreContext.mPortsInUse[p],
                                    &sem,
                                    -1 /*Infinite timeout*/
                                    );

        if( OMX_ErrorNone == omxError )
        {
            omxError = OMX_SendCommand( mCurGreContext.mHandleComp,
                                        OMX_CommandFlush,
                                        mCurGreContext.mPortsInUse[p],
                                        NULL );
        }

        if( OMX_ErrorNone == omxError )
        {
            sem_wait(&sem);
        }

        sem_destroy(&sem);
    }
    return greError;
}

/* Send command to Camera */
int OMXVisionCam::sendCommand( VisionCamCmd_e cmdId, void *param, uint32_t size, VisionCamPort_e port)
{
    int greError = 0;
    OMX_ERRORTYPE omxError = OMX_ErrorNone;

    AutoLock lock(&mUserRequestLock);

    MSG("SEND CMD: 0x%04x, %p, %zu, %d", cmdId, param, size, port);

    switch (cmdId)
    {
        case VCAM_CMD_PREVIEW_START:
        {
            greError = startPreview(port);
            break;
        }

        case VCAM_CMD_PREVIEW_STOP:
        {
            if( OMX_StateExecuting == getComponentState() )
                greError = stopPreview(port);
            else
                greError = -EBADE;
            break;
        }

        case VCAM_EXTRA_DATA_START:
        {
            int i, EDataType;
            for( i = 0; i < VCAM_EXTRA_DATA_TYPE_MAX; i++ )
            {
                if( ExtraDataTypeLUT[ i ][ 0 ]  == *( (int*)param ) )
                {
                    EDataType = ExtraDataTypeLUT[ i ][ 1 ];
                    break;
                }
            }
            if( i == VCAM_EXTRA_DATA_TYPE_MAX )
            {
                greError = -EINVAL;
                break;
            }

            OMX_CONFIG_EXTRADATATYPE xData;
            OMX_STRUCT_INIT(xData, OMX_CONFIG_EXTRADATATYPE, mLocalVersion);
#if defined(TUNA) || defined(MAGURO)
            xData.eCameraView       = OMX_2D_Prv;
#endif
            if( OMX_ExtraDataNone == EDataType )
            {
              for( int i = 1; i < VCAM_EXTRA_DATA_TYPE_MAX; i++ )
              {// stop extra data transfer for all types of data
                  if( 0 == greError )
                  {
                    xData.eExtraDataType = (OMX_EXT_EXTRADATATYPE)ExtraDataTypeLUT[i][1];
                    xData.bEnable = OMX_FALSE;
                    int32_t p;
                    LOOP_PORTS( port, p )
                    {
                        xData.nPortIndex = p;
                        omxError = OMX_SetConfig( mCurGreContext.mHandleComp, ( OMX_INDEXTYPE )OMX_IndexConfigOtherExtraDataControl, &xData);
                    }
                  }
              }
            }
            else
            {
                if( 0 == greError )
                {
                    xData.eExtraDataType = ( OMX_EXT_EXTRADATATYPE )EDataType;
                    xData.bEnable = OMX_TRUE;

                    omxError = OMX_SetConfig( mCurGreContext.mHandleComp, ( OMX_INDEXTYPE )OMX_IndexConfigOtherExtraDataControl, &xData);
                }
            }
            break;
        }

        case VCAM_EXTRA_DATA_STOP:
        {
            int i, EDataType;
            for( i = 0; i < VCAM_EXTRA_DATA_TYPE_MAX; i++ )
            {
                if( ExtraDataTypeLUT[ i ][ 0 ]  == *( (int*)param ) )
                {
                    EDataType = ExtraDataTypeLUT[ i ][ 1 ];
                    break;
                }
            }
            if( i == VCAM_EXTRA_DATA_TYPE_MAX )
            {
                greError = -EINVAL;
                break;
            }

            OMX_CONFIG_EXTRADATATYPE xData;
            OMX_STRUCT_INIT(xData, OMX_CONFIG_EXTRADATATYPE, mLocalVersion);
#if defined(TUNA) || defined(MAGURO)
            xData.eCameraView       = OMX_2D_Prv;
#endif
            if( OMX_ExtraDataNone == EDataType )
            {
                  for( int i = 1; i < VCAM_EXTRA_DATA_TYPE_MAX; i++ )
                  {
    //                   greError = OMX_GetConfig( mCurGreContext.mHandleComp, ( OMX_INDEXTYPE )OMX_IndexConfigOtherExtraDataControl, &xData);
                      if( 0 == greError )
                      {
                          xData.eExtraDataType = (OMX_EXT_EXTRADATATYPE)ExtraDataTypeLUT[i][1];
                          xData.bEnable = OMX_FALSE;
                          int32_t p;
                          LOOP_PORTS(port, p)
                          {
                                xData.nPortIndex = p;
                                omxError = OMX_SetConfig( mCurGreContext.mHandleComp, ( OMX_INDEXTYPE )OMX_IndexConfigOtherExtraDataControl, &xData);
                          }
                      }
                  }
            }
            else
            {
                if( 0 == greError )
                {
                    xData.eExtraDataType = ( OMX_EXT_EXTRADATATYPE )EDataType;
//                     greError = OMX_GetConfig( mCurGreContext.mHandleComp, ( OMX_INDEXTYPE )OMX_IndexConfigOtherExtraDataControl, &xData);

                    xData.bEnable = OMX_FALSE;

                    omxError = OMX_SetConfig( mCurGreContext.mHandleComp, ( OMX_INDEXTYPE )OMX_IndexConfigOtherExtraDataControl, &xData);
                }
            }
            break;
        }

        case VCAM_CMD_LOCK_AE:
        case VCAM_CMD_LOCK_AWB:
        {
            OMX_INDEXTYPE index;
            OMX_IMAGE_CONFIG_LOCKTYPE lockCfg;
            OMX_STRUCT_INIT(lockCfg, OMX_IMAGE_CONFIG_LOCKTYPE, mLocalVersion);
            if (VCAM_CMD_LOCK_AE == cmdId)
            {
                index = (OMX_INDEXTYPE)OMX_IndexConfigImageExposureLock;
            }
            else if (VCAM_CMD_LOCK_AWB == cmdId)
            {
                index = (OMX_INDEXTYPE)OMX_IndexConfigImageWhiteBalanceLock;
            }
            else
            {
                greError = -EINVAL;
                break;
            }
            OMX_GetConfig(mCurGreContext.mHandleComp, index, &lockCfg);
            lockCfg.bLock = *((OMX_BOOL*)param);
            OMX_SetConfig(mCurGreContext.mHandleComp, index, &lockCfg);
            break;
        }
#if TIME_PROFILE
        case VCAM_DUMP_TIMES:
        {
            for( int i = 0; i < VCAM_TIME_TARGET_MAX; i++ )
                if( mTimeProfiler[i] )
                    mTimeProfiler[i]->dump();
            break;
        }
#endif // TIME_PROFILE
        case VCAM_CMD_FACE_DETECTION:
        {
            mFaceDetectionEnabled = *((bool*)(param));
            enableFaceDetect(port);
            break;
        }
#if ( defined(DUCATI_1_5) || defined(DUCATI_2_0) ) && defined(OMX_CAMERA_SUPPORTS_MANUAL_CONTROLS)
        case VCAM_CMD_FREEZE_AWB_PARAMS:
        {
            OMX_TI_CONFIG_FREEZE_AWB wbFreeze;
            OMX_STRUCT_INIT( wbFreeze, OMX_TI_CONFIG_FREEZE_AWB, mLocalVersion);
            wbFreeze.nTimeDelay = *((uint32_t*)param);

            omxError = OMX_SetConfig( mCurGreContext.mHandleComp, (OMX_INDEXTYPE)OMX_TI_IndexConfigFreezeAWB, &wbFreeze );
            break;
        }

        case VCAM_CMD_FREEZE_AGC_PARAMS:
        {
            OMX_TI_CONFIG_FREEZE_AE aeFreeze;
            OMX_STRUCT_INIT( aeFreeze, OMX_TI_CONFIG_FREEZE_AE, mLocalVersion);
            aeFreeze.nTimeDelay = *((uint32_t*)param);

            omxError = OMX_SetConfig( mCurGreContext.mHandleComp, (OMX_INDEXTYPE)OMX_TI_IndexConfigFreezeAutoExp, &aeFreeze );
            break;
        }
#endif
        case VCAM_CMD_SET_CLIENT_NOTIFICATION_CALLBACK:
        {
            mClientNotifier.mNotificationCallback = (VisionCamClientNotifier::VisionCamClientNotifierCallback)(param);
            break;
        }

        case VCAM_CMD_PACK_FRAMES:
        {
            mUseFramePackaging = *((bool*)param);
            break;
        }

        default:
        {
            ERROR("Impossible command id requested: %d", cmdId);
            ERROR("see VisionCamParam_e for possible command ids");
            greError = -EINVAL;
        }
    }

    if (greError == 0)
        greError = ConvertError(omxError);

    if (greError != 0)
    {
        ERROR("OMXVisionCam::sendCommand() exits with error %d for command id %d", greError, cmdId);
    }

    return greError;
}

/*
*  APIs to configure Vision Cam
*/
int OMXVisionCam::setParameter( VisionCamParam_e paramId, void* param, uint32_t size, VisionCamPort_e port)
{
    int greError = 0;
    OMX_ERRORTYPE omxError = OMX_ErrorNone;

    AutoLock lock(&mUserRequestLock);

    if (param == NULL || size == 0 || port >= VCAM_PORT_MAX)
    {
        ERROR("NULL param pointer passed to %s()",__func__);
        return -EINVAL;
    }
    else
    {
        MSG("SET PARAM: 0x%04x, %p, %zu (0x%08x), %d", paramId, param, size, (size==4?*(uint32_t *)param:0), port);
    }

    if (getComponentState() == OMX_StateInvalid)
        return -EBADE;

    switch( paramId )
    {
        case VCAM_PARAM_HEIGHT:
        {
            int32_t p;
            LOOP_PORTS( port , p )
                mCurGreContext.mCameraPortParams[p].mHeight = *((OMX_U32*)param);
            break;
        }

        case VCAM_PARAM_WIDTH:
        {
            int32_t p;
            LOOP_PORTS( port , p )
                    mCurGreContext.mCameraPortParams[p].mWidth = *((OMX_U32*)param);
            break;
        }


        case VCAM_PARAM_COLOR_SPACE_FOURCC:
        {
            uint32_t color = *((uint32_t*)param);
            switch (color) {
                case FOURCC('U','Y','V','Y'):
                case FOURCC('Y','U','Y','V'):
                    mCurGreContext.mCameraPortParams[port].mColorFormat =
                        OMX_COLOR_FormatCbYCrY;
                    break;
                case FOURCC('N','V','1','2'):
                    mCurGreContext.mCameraPortParams[port].mColorFormat =
                        OMX_COLOR_FormatYUV420SemiPlanar;
                    break;
                default:
                    greError = -EINVAL;
            }
            if (greError == 0) {
                int32_t p;
                LOOP_PORTS(port , p) {
                    OMX_PARAM_PORTDEFINITIONTYPE portCheck;
                    omxError = initPortCheck( &portCheck, p );
                    portCheck.format.video.eColorFormat =
                        mCurGreContext.mCameraPortParams[port].mColorFormat;
                    omxError = OMX_SetParameter(mCurGreContext.mHandleComp,
                        OMX_IndexParamPortDefinition, &portCheck);
                }
            }
            break;
        }

        case VCAM_PARAM_DO_AUTOFOCUS:
        {
            if (OMX_StateExecuting == getComponentState())
            {
                greError = startAutoFocus( *((VisionCamFocusMode*)param) );
            }
            else
            {
                greError = ConvertError(OMX_ErrorIncorrectStateOperation);
            }
            break;
        }

        case VCAM_PARAM_DO_MANUALFOCUS:
        {
            if (OMX_StateExecuting == getComponentState())
            {
                mManualFocusDistance = *((uint32_t*)param);
                greError = startAutoFocus( VCAM_FOCUS_CONTROL_ON );
            }
            else
            {
                greError = ConvertError(OMX_ErrorIncorrectStateOperation);
            }
            break;
        }

        case VCAM_PARAM_BRIGHTNESS:
        {
            OMX_CONFIG_BRIGHTNESSTYPE brightness;
            brightness.nSize = sizeof(OMX_CONFIG_BRIGHTNESSTYPE);
            brightness.nBrightness = *((int*)param);
            memcpy( &brightness.nVersion, mLocalVersion, sizeof(mLocalVersion) );

            int32_t p = port;
//            LOOP_PORTS( port , p )
            {
                brightness.nPortIndex = mCurGreContext.mPortsInUse[p];
                omxError = OMX_SetConfig( mCurGreContext.mHandleComp, OMX_IndexConfigCommonBrightness, &brightness);
            }
            break;
        }

        case VCAM_PARAM_CONTRAST:
        {
            OMX_CONFIG_CONTRASTTYPE contrast;
            contrast.nSize = sizeof( OMX_CONFIG_CONTRASTTYPE );
            contrast.nContrast = *((int*)param);
            memcpy( &contrast.nVersion, mLocalVersion, sizeof(mLocalVersion) );

            int32_t p = port;
//            LOOP_PORTS( port , p )
            {
                contrast.nPortIndex = mCurGreContext.mPortsInUse[p];
                omxError = OMX_SetConfig( mCurGreContext.mHandleComp, OMX_IndexConfigCommonContrast, &contrast);
            }
            break;
        }

        case VCAM_PARAM_SHARPNESS:
        {
            OMX_IMAGE_CONFIG_PROCESSINGLEVELTYPE procSharpness;
            procSharpness.nSize = sizeof( OMX_IMAGE_CONFIG_PROCESSINGLEVELTYPE );
            procSharpness.nLevel = *((int*)param);
            memcpy( &procSharpness.nVersion, mLocalVersion, sizeof(mLocalVersion) );

            if( procSharpness.nLevel == 0 )
                procSharpness.bAuto = OMX_TRUE;
            else
                procSharpness.bAuto = OMX_FALSE;

            int32_t p = port;
//            LOOP_PORTS( port , p )
            {
                procSharpness.nPortIndex = mCurGreContext.mPortsInUse[p];
                omxError = OMX_SetConfig( mCurGreContext.mHandleComp, (OMX_INDEXTYPE)OMX_IndexConfigSharpeningLevel, &procSharpness);
            }
            break;
        }

        case VCAM_PARAM_SATURATION:
        {
            OMX_CONFIG_SATURATIONTYPE saturation;
            saturation.nSize = sizeof(OMX_CONFIG_SATURATIONTYPE);
            saturation.nSaturation = *((int*)param);
            memcpy( &saturation.nVersion, mLocalVersion, sizeof(mLocalVersion) );

            int32_t p = port;
//            LOOP_PORTS( port , p )
            {
                saturation.nPortIndex = mCurGreContext.mPortsInUse[p];
                omxError = OMX_SetConfig( mCurGreContext.mHandleComp,
                                          OMX_IndexConfigCommonSaturation,
                                          &saturation
                                        );
            }
            break;
        }

        case VCAM_PARAM_FPS_FIXED:
        {
            bool enableAgain[VCAM_PORT_MAX];

            int32_t p;
            LOOP_PORTS(VCAM_PORT_ALL , p)
                enableAgain[p] = mCurGreContext.mCameraPortParams[p].mIsActive;

            stopPreview(VCAM_PORT_ALL);
            int32_t i;
            LOOP_PORTS(VCAM_PORT_ALL , i)
            {
                mCurGreContext.mCameraPortParams[i].mFrameRate = (*((int*)param));
                if( enableAgain[i] )
                    startPreview( (VisionCamPort_e)i );
            }

            break;
        }

        case VCAM_PARAM_FPS_VAR:
        {
            VisionCamVarFramerateType varFrate;
            memcpy( &varFrate , param , sizeof( VisionCamVarFramerateType ) );

            bool enableAgain[VCAM_PORT_MAX];

            int32_t p;
            LOOP_PORTS( VCAM_PORT_ALL , p )
            {
                enableAgain[p] = mCurGreContext.mCameraPortParams[p].mIsActive;
            }

            stopPreview(VCAM_PORT_ALL);

            if( varFrate.mMin != 0 )
            {
              // @todo implement when needed OMX interface is present
                varFrate = varFrate;
            }

            int32_t i;
            LOOP_PORTS( VCAM_PORT_ALL , i )
            {
                mCurGreContext.mCameraPortParams[i].mFrameRate = varFrate.mMax;
                if( enableAgain[i] )
                    startPreview( (VisionCamPort_e)i );
            }
            break;
        }

        case VCAM_PARAM_FLICKER:
        {
            OMX_CONFIG_FLICKERCANCELTYPE flicker;
            flicker.nSize = sizeof( OMX_CONFIG_FLICKERCANCELTYPE );
            memcpy( &flicker.nVersion, mLocalVersion, sizeof(mLocalVersion) );
            flicker.eFlickerCancel = *(OMX_COMMONFLICKERCANCELTYPE *)param;

            int32_t p = port;
//            LOOP_PORTS( VCAM_PORT_ALL , p )
            {
                flicker.nPortIndex = mCurGreContext.mPortsInUse[p];
                omxError = OMX_SetConfig( mCurGreContext.mHandleComp, (OMX_INDEXTYPE)OMX_IndexConfigFlickerCancel, &flicker );
            }
            break;
        }

        case VCAM_PARAM_CROP:
        {
            OMX_CONFIG_RECTTYPE crop;
            crop.nSize = sizeof( OMX_CONFIG_RECTTYPE );
            crop.nLeft = ((VisionCamRectType*)param)->mLeft;
            crop.nTop = ((VisionCamRectType*)param)->mTop;
            crop.nWidth = ((VisionCamRectType*)param)->mWidth;
            crop.nHeight = ((VisionCamRectType*)param)->mHeight;
            memcpy( &crop.nVersion,  mLocalVersion, sizeof(mLocalVersion) );

            int32_t p = port;
//            LOOP_PORTS( port , p )
            {
                crop.nPortIndex = mCurGreContext.mPortsInUse[p];
                omxError = OMX_SetConfig( mCurGreContext.mHandleComp, OMX_IndexConfigCommonOutputCrop, &crop );
            }
            break;
        }

        case VCAM_PARAM_STEREO_INFO:
        {
            VisionCamStereoInfo *info = (VisionCamStereoInfo *)param;
            OMX_TI_FRAMELAYOUTTYPE frameLayout;
            OMX_STRUCT_INIT(frameLayout, OMX_TI_FRAMELAYOUTTYPE, mLocalVersion);
            OMX_TI_CONFIG_CONVERGENCETYPE acParams;
            OMX_STRUCT_INIT(acParams, OMX_TI_CONFIG_CONVERGENCETYPE, mLocalVersion);
            if ( info && size == sizeof(VisionCamStereoInfo))
            {
                int32_t p = port;
//                LOOP_PORTS( port , p )
                {
                    frameLayout.nPortIndex = mCurGreContext.mPortsInUse[p];
                    MSG("Stereo Info: Layout %u, SubSampling: %u", info->layout, info->subsampling);

                    frameLayout.eFrameLayout = (OMX_TI_STEREOFRAMELAYOUTTYPE)getLutValue( (int)info->layout, VCAM_VALUE_TYPE,
                                                                                            StereoLayoutLUT, ARR_SIZE(StereoLayoutLUT)
                                                                                        );
                    if (info->layout == VCAM_STEREO_LAYOUT_TOPBOTTOM)
                        frameLayout.eFrameLayout = OMX_TI_StereoFrameLayoutTopBottom;
                    else if (info->layout == VCAM_STEREO_LAYOUT_LEFTRIGHT)
                        frameLayout.eFrameLayout = OMX_TI_StereoFrameLayoutLeftRight;
                    frameLayout.nSubsampleRatio = info->subsampling << 7; // in Q15.7 format
                    omxError = OMX_SetParameter( mCurGreContext.mHandleComp,
                                                 (OMX_INDEXTYPE)OMX_TI_IndexParamStereoFrmLayout,
                                                 &frameLayout
                                                );

                    if( OMX_ErrorNone == omxError )
                    {
                        acParams.nPortIndex = mCurGreContext.mPortsInUse[p];

                        acParams.nManualConverence = 0;
                        acParams.eACMode = OMX_TI_AutoConvergenceModeDisable;

                        omxError = OMX_SetConfig( mCurGreContext.mHandleComp,
                                                  (OMX_INDEXTYPE)OMX_TI_IndexConfigAutoConvergence,
                                                  &acParams
                                                 );
                        if( OMX_ErrorNone != omxError )
                        {
                            ERROR("ERROR OMXVisionCam: Failed to disable auto convergence.");
                        }
                    }
                    else
                    {
                        ERROR("ERROR OMXVisionCam: Failed to set stero layout");
                    }
                }
            }
            else
            {
                ERROR("ERROR OMXVisionCam: attempt to set param with id VCAM_PARAM_STEREO_INFO with an invalid args");
                greError = -EINVAL;
            }
            break;
        }

        case VCAM_PARAM_CAP_MODE:
        {
            if (param != NULL)
            {
                VisionCamCaptureMode mode = *(VisionCamCaptureMode *)param;

                OMX_CONFIG_CAMOPERATINGMODETYPE opMode;
                opMode.nSize = sizeof( OMX_CONFIG_CAMOPERATINGMODETYPE );
                memcpy( &opMode.nVersion, mLocalVersion, sizeof(mLocalVersion) );

                opMode.eCamOperatingMode = (OMX_CAMOPERATINGMODETYPE)getLutValue(mode, VCAM_VALUE_TYPE, CaptureModeLUT, ARR_SIZE(CaptureModeLUT) );

                MSG("Requested VisionCamCaptureMode %d", mode);
                MSG("Requested OMX_CAMOPERATINGMODETYPE %d", opMode.eCamOperatingMode);

                bool enableAgain[VCAM_PORT_MAX];

                int32_t p;// mark and stop all working ports
                LOOP_PORTS( VCAM_PORT_ALL , p )
                {
                    enableAgain[p] = mCurGreContext.mCameraPortParams[p].mIsActive;
                    if( mCurGreContext.mCameraPortParams[p].mIsActive )
                        portEnableDisable(OMX_CommandPortDisable, freePortBuffersSrvc, (VisionCamPort_e)p );
                }

                int32_t oldstate = getComponentState();
                for(int32_t st = oldstate - 1; st >= OMX_StateLoaded; st--)
                {
                    if( OMX_StatePause == oldstate  && OMX_StateExecuting == st )
                        continue;

                    transitToState( (OMX_STATETYPE)st );
                }

                if( OMX_StateLoaded == getComponentState() || OMX_StateWaitForResources == getComponentState() )
                {
                    omxError = OMX_SetParameter( mCurGreContext.mHandleComp,
                                                 (OMX_INDEXTYPE)OMX_IndexCameraOperatingMode,
                                                 &opMode );

                    greError = ConvertError(omxError);
                }

                for(int32_t st = (1 + getComponentState()); st <= oldstate; st++)
                {
                    if( OMX_StatePause == oldstate && OMX_StateExecuting == st)
                        continue;

                    transitToState( (OMX_STATETYPE)st );
                }

                int32_t por;
                LOOP_PORTS( VCAM_PORT_ALL , por )
                    if( enableAgain[por])
                    {
                        portEnableDisable(OMX_CommandPortEnable, populatePortSrvc, (VisionCamPort_e)por);
                        fillPortBuffers((VisionCamPort_e)por);
                    }
            }
            else
                greError = -EINVAL;
            break;
        }

        case VCAM_PARAM_SENSOR_SELECT:
        {
            if (param != NULL)
            {
                OMX_CONFIG_SENSORSELECTTYPE sensor;
                OMX_STRUCT_INIT(sensor, OMX_CONFIG_SENSORSELECTTYPE, mLocalVersion);
                sensor.nPortIndex = mCurGreContext.mPortsInUse[VCAM_PORT_ALL];
                sensor.eSensor = *((OMX_SENSORSELECT*)param);
                MSG("Selecting sensor index = %u", sensor.eSensor);
                omxError = OMX_SetConfig( mCurGreContext.mHandleComp, (OMX_INDEXTYPE)OMX_TI_IndexConfigSensorSelect, &sensor);
            }
            else
                greError = -EINVAL;
            break;
        }

        case VCAM_PARAM_EXPOSURE_COMPENSATION:
        {
            OMX_CONFIG_EXPOSUREVALUETYPE expValues;
            OMX_STRUCT_INIT(expValues, OMX_CONFIG_EXPOSUREVALUETYPE, mLocalVersion);

            memcpy( &expValues.nVersion , mLocalVersion , sizeof( *mLocalVersion ) );
            omxError = OMX_GetConfig( mCurGreContext.mHandleComp , OMX_IndexConfigCommonExposureValue , &expValues );

            if( OMX_ErrorNone == omxError )
            {
                int compVal = *((int*)param);
                expValues.xEVCompensation = ( compVal * ( 1 << 16 ) )  / 10;

                int32_t p = port;
//                LOOP_PORTS( port , p )
                {
                    expValues.nPortIndex = mCurGreContext.mPortsInUse[p];
                    omxError = OMX_SetConfig( mCurGreContext.mHandleComp , OMX_IndexConfigCommonExposureValue , &expValues );
                }
            }
            break;
        }

        case VCAM_PARAM_RESOLUTION:
        {
            if( param != NULL )
            {
                int res;

                if( *((int*)param) < 0 || *((int*)param) >= VCAM_RESOL_MAX )
                {
                   greError = -EINVAL;
                   break;
                }

                int32_t p = port;
                LOOP_PORTS( port , p )
                {
                    res = *((int*)param);
                    if( 0 == greError )
                        if( mCurGreContext.mCameraPortParams[p].mIsActive )
                            greError = -EBADE;

                    if( 0 == greError )
                    {
                            if( VCAM_PORT_VIDEO == p
                                && ( VisionCamResolutions[res].mWidth > VisionCamResolutions[VCAM_RES_VGA].mWidth
                                || VisionCamResolutions[res].mHeight > VisionCamResolutions[VCAM_RES_VGA].mHeight ) )
                            {
                                res = VCAM_RES_VGA;
                            }

                        mCurGreContext.mCameraPortParams[p].mWidth = VisionCamResolutions[res].mWidth;
                        mCurGreContext.mCameraPortParams[p].mHeight = VisionCamResolutions[res].mHeight;
                    }
                }
            }
            else
                greError = -EINVAL;

            break;
        }

        case VCAM_PARAM_MANUAL_EXPOSURE:
        {
            OMX_CONFIG_EXPOSUREVALUETYPE expValues;
            OMX_STRUCT_INIT(expValues, OMX_CONFIG_EXPOSUREVALUETYPE, mLocalVersion);
            int32_t p = port;

//            LOOP_PORTS( port , p )
            {
                expValues.nPortIndex = mCurGreContext.mPortsInUse[p];
                omxError = OMX_GetConfig( mCurGreContext.mHandleComp , OMX_IndexConfigCommonExposureValue , &expValues );

                if( OMX_ErrorNone == omxError )
                {
                    if( *(OMX_U32*)param )
                    {
                        expValues.nShutterSpeedMsec = *(OMX_U32*)param;
                        expValues.bAutoShutterSpeed = OMX_FALSE;
                    }
                    else
                        expValues.bAutoShutterSpeed = OMX_TRUE;

                    omxError = OMX_SetConfig( mCurGreContext.mHandleComp , OMX_IndexConfigCommonExposureValue , &expValues );
                }
            }
            break;
        }

        case VCAM_PARAM_EXPOSURE_ISO:
        {
            OMX_CONFIG_EXPOSUREVALUETYPE expValues;
            OMX_STRUCT_INIT(expValues, OMX_CONFIG_EXPOSUREVALUETYPE, mLocalVersion);
            int32_t p = port;
//            LOOP_PORTS( port , p )
            {

                expValues.nPortIndex = mCurGreContext.mPortsInUse[p];
                omxError = OMX_GetConfig(mCurGreContext.mHandleComp, OMX_IndexConfigCommonExposureValue, &expValues);
                if( OMX_ErrorNone == omxError )
                {
                    if( *(OMX_U32*)param )
                    {
                        expValues.nSensitivity = *(OMX_U32*)param;
                        expValues.bAutoSensitivity = OMX_FALSE;
                    }
                    else
                        expValues.bAutoSensitivity = OMX_TRUE;

                    omxError = OMX_SetConfig( mCurGreContext.mHandleComp , OMX_IndexConfigCommonExposureValue , &expValues );
                }
            }
            break;
        }

        case VCAM_PARAM_AWB_MODE:
        {
            OMX_CONFIG_WHITEBALCONTROLTYPE wb;
            OMX_STRUCT_INIT(wb, OMX_CONFIG_WHITEBALCONTROLTYPE, mLocalVersion);

            int32_t p = port;
//            LOOP_PORTS( port , p )
            {
                wb.nPortIndex = mCurGreContext.mPortsInUse[p];
                wb.eWhiteBalControl = *(OMX_WHITEBALCONTROLTYPE *)param;
                omxError = OMX_SetConfig( mCurGreContext.mHandleComp,
                                        OMX_IndexConfigCommonWhiteBalance,
                                        &wb);
            }
            break;
        }

        case VCAM_PARAM_COLOR_TEMP:
        {
            OMX_CONFIG_WHITEBALCONTROLTYPE wb;
            OMX_STRUCT_INIT(wb, OMX_CONFIG_WHITEBALCONTROLTYPE, mLocalVersion);

            if( 0 == *(int*)param )
                wb.eWhiteBalControl = OMX_WhiteBalControlAuto;
            else
                wb.eWhiteBalControl = OMX_WhiteBalControlOff; // @todo change to manual when proper OMX headers arrive

            int32_t p = port;
//            LOOP_PORTS( port , p )
            {
                wb.nPortIndex = mCurGreContext.mPortsInUse[p];
                omxError = OMX_SetConfig( mCurGreContext.mHandleComp,
                                     OMX_IndexConfigCommonWhiteBalance,
                                     &wb );
            }
            break;
        }

        case VCAM_PARAM_WB_COLOR_GAINS:
        {
#ifdef USE_WB_GAIN_PATCH
            VisionCamWhiteBalGains wbGains = *((VisionCamWhiteBalGains*)param);
            uint16_t * tmp;
            CALCULATE_WB_GAINS_OFFSET(uint16_t,mWBbuffer,tmp);

            tmp[ RED ] = wbGains.mRed;
            tmp[ GREEN_RED ] = wbGains.mGreen_r;
            tmp[ GREEN_BLUE ] = wbGains.mGreen_b;
            tmp[ BLUE ] = wbGains.mBlue;

            OMX_TI_CONFIG_SHAREDBUFFER skipBuffer;
            skipBuffer.nSize = sizeof( OMX_TI_CONFIG_SHAREDBUFFER );
            memcpy( &skipBuffer.nVersion , mLocalVersion , sizeof(mLocalVersion) );

            if( wbGains.mRed >= COLOR_GAIN_MIN &&  wbGains.mRed <= COLOR_GAIN_MAX
                && wbGains.mGreen_b >= COLOR_GAIN_MIN && wbGains.mGreen_b <= COLOR_GAIN_MAX
                && wbGains.mGreen_r >= COLOR_GAIN_MIN && wbGains.mGreen_r <= COLOR_GAIN_MAX
                && wbGains.mBlue >= COLOR_GAIN_MIN && wbGains.mBlue <= COLOR_GAIN_MAX )
            {
                skipBuffer.pSharedBuff = (OMX_U8*)mWBbuffer;
                skipBuffer.nSharedBuffSize = sizeof(mWBbuffer);
            }
            else if( !wbGains.mRed && !wbGains.mGreen_b && !wbGains.mGreen_r && !wbGains.mBlue )
            {   /// all gains are zero => auto mode
                skipBuffer.pSharedBuff = (OMX_U8*)mWBresetBuffer;
                skipBuffer.nSharedBuffSize = sizeof(mWBresetBuffer);
            }
            else
            {
                greError = -EINVAL;
                break;
            }

            int32_t p = port;
//            LOOP_PORTS(port, p)
            {
                skipBuffer.nPortIndex = p;
                omxError = OMX_SetConfig( mCurGreContext.mHandleComp,
                                        (OMX_INDEXTYPE) OMX_TI_IndexConfigAAAskipBuffer,
                                        &skipBuffer );
            }
#endif // USE_WB_GAIN_PATCH
            break;
        }

        case VCAM_PARAM_GAMMA_TBLS:
        {
            VisionCamGammaTableType *gammaTbl = (VisionCamGammaTableType*)(param);
            OMX_TI_CONFIG_SHAREDBUFFER skipBuffer;
            OMX_STRUCT_INIT(skipBuffer, OMX_TI_CONFIG_SHAREDBUFFER, mLocalVersion);

            uint32_t* base = (uint32_t *)(mGammaTablesBuf + 12); // 12 bytes offset for first table
            uint16_t *redTbl      = (uint16_t*)(base[0] + (uint32_t)&base[2]);
            uint16_t *blueTbl     = (uint16_t*)(base[1] + (uint32_t)&base[2]);
            uint16_t *greenTbl    = (uint16_t*)(base[2] + (uint32_t)&base[2]);

#ifdef _USE_GAMMA_RESET_HC_
            if( !mGammaResetPolulated )
            {
                skipBuffer.pSharedBuff = (OMX_U8*)mGammaResetTablesBuf;
                skipBuffer.nSharedBuffSize = sizeof(mGammaResetTablesBuf);
                omxError = OMX_GetConfig( mCurGreContext.mHandleComp,
                                    (OMX_INDEXTYPE) OMX_TI_IndexConfigAAAskipBuffer,
                                    &skipBuffer );

                if( OMX_ErrorNone == omxError )
                    mGammaResetPolulated = true;
            }
#endif // _USE_GAMMA_RESET_HC_

            if( gammaTbl->mRedTable && gammaTbl->mGreenTable && gammaTbl->mBlueTable )
            {
                if( gammaTbl->mRedTable != redTbl )
                {
                    memcpy(redTbl, gammaTbl->mRedTable, GAMMA_TABLE_SIZE );
                }

                if( gammaTbl->mGreenTable != greenTbl )
                {
                    memcpy(greenTbl, gammaTbl->mGreenTable, GAMMA_TABLE_SIZE );
                }

                if( gammaTbl->mBlueTable != blueTbl )
                {
                    memcpy(blueTbl, gammaTbl->mBlueTable, GAMMA_TABLE_SIZE );
                }

                skipBuffer.pSharedBuff = (OMX_U8*)mGammaTablesBuf;
                skipBuffer.nSharedBuffSize = sizeof(mGammaTablesBuf)/sizeof(mGammaTablesBuf[0]);
            }
            else
            {
#ifdef _USE_GAMMA_RESET_HC_
                if( mGammaResetPolulated )
                {
                    skipBuffer.pSharedBuff = (OMX_U8*)mGammaResetTablesBuf;
                    skipBuffer.nSharedBuffSize = sizeof(mGammaResetTablesBuf)/sizeof(mGammaResetTablesBuf[0]);
                }
                else
                {
                    ERROR("No data present in reset Gamma Tables. Leaving Gamma unchanged!!!");
                }
#else
                skipBuffer.pSharedBuff = (OMX_U8*)mGammaResetTablesBuf;
                skipBuffer.nSharedBuffSize = sizeof(mGammaResetTablesBuf)/sizeof(mGammaResetTablesBuf[0]);
#endif // _USE_GAMMA_RESET_HC_
            }

            omxError = OMX_SetConfig( mCurGreContext.mHandleComp,
                                    (OMX_INDEXTYPE) OMX_TI_IndexConfigAAAskipBuffer,
                                    &skipBuffer );

            break;
        }

#if defined(VCAM_SET_FORMAT_ROTATION)
        case VCAM_PARAM_ROTATION:
        {
            int32_t p;
            LOOP_PORTS( port , p )
            {
                mCurGreContext.mCameraPortParams[p].mRotation = *((OMX_S32 *)param);
            }
            break;
        }
#else
        case VCAM_PARAM_ROTATION:
        {
            OMX_CONFIG_ROTATIONTYPE rotation;
            OMX_STRUCT_INIT(rotation, OMX_CONFIG_ROTATIONTYPE, mLocalVersion);
            rotation.nRotation = *((OMX_S32*)param);

            int32_t p;
            LOOP_PORTS( port , p )
            {
                rotation.nPortIndex = mCurGreContext.mPortsInUse[p];
                OMX_CHECK(omxError, OMX_SetConfig(mCurGreContext.mHandleComp,
                                                  OMX_IndexConfigCommonRotate,
                                                  &rotation));
                MSG("Setting Rotation to %ld (size: %u)", rotation.nRotation, sizeof(rotation));
            }
            break;
        }
#endif
        case VCAM_PARAM_MIRROR:
        {
            int i;
            OMX_CONFIG_MIRRORTYPE mirror;
            OMX_STRUCT_INIT(mirror, OMX_CONFIG_MIRRORTYPE, mLocalVersion);

            for( i = 0; i < VCAM_MIRROR_MAX; i++ )
            {
                if( MirrorTypeLUT[i][0] == *((VisionCamMirrorType*)param) )
                {
                  mirror.eMirror = (OMX_MIRRORTYPE) MirrorTypeLUT[i][1];
                  break;
                }
            }

            if( i < VCAM_MIRROR_MAX )
            {
                int32_t p;
                LOOP_PORTS( port , p )
                {
                    mirror.nPortIndex = mCurGreContext.mPortsInUse[p];
                    omxError = OMX_SetConfig( mCurGreContext.mHandleComp,
                                            OMX_IndexConfigCommonMirror,
                                            &mirror );
                }
            }
            else
                greError = -EINVAL;
            break;
        }

#if ( defined(DUCATI_1_5) || defined(DUCATI_2_0) ) && defined(OMX_CAMERA_SUPPORTS_MANUAL_CONTROLS)
        case VCAM_PARAM_AWB_MIN_DELAY_TIME:
        {
            uint32_t timeDelay = *((uint32_t*)param);
            if( timeDelay > AE_Delay_Time_Max )
            {
                greError = -EINVAL;
            }
            else
            {
                OMX_TI_CONFIG_AE_DELAY aeDelay;
                OMX_STRUCT_INIT( aeDelay, OMX_TI_CONFIG_AE_DELAY, mLocalVersion );
                aeDelay.nDelayTime = timeDelay;

                int32_t p = port;
//                LOOP_PORTS( port , p )
                {
                    aeDelay.nPortIndex = p;
                    omxError = OMX_SetConfig( mCurGreContext.mHandleComp,
                                                ( OMX_INDEXTYPE )OMX_TI_IndexConfigAutoExpMinDelayTime,
                                                &aeDelay
                                            );
                }
            }
            break;
        }
#endif
#if ( defined(DUCATI_1_5) || defined(DUCATI_2_0) ) && defined(OMX_CAMERA_SUPPORTS_GESTURES)
        case VCAM_PARAM_GESTURES_INFO:
        {
            VisionCamGestureInfo *info = (VisionCamGestureInfo*)param;
            if( info->mGestureType >= VCAM_GESTURE_EVENT_MAX || info->mGestureType < VCAM_GESTURE_EVENT_INVALID )
            {
                greError= -EINVAL;
            }

            if( info->mRegionsNum >= VCAM_Max_Gesture_Per_Frame )
            {
                greError= -EINVAL;
            }

            if( 0 == greError )
            {
                OMX_TI_CONFIG_GESTURES_INFO gestInfo;
                OMX_STRUCT_INIT( gestInfo, OMX_TI_CONFIG_GESTURES_INFO , mLocalVersion );

                gestInfo.nTimeStamp = info->timeStamp;
                gestInfo.nNumDetectedGestures = info->mRegionsNum;
                gestInfo.eType = (OMX_TI_GESTURES_TYPE)getLutValue( info->mGestureType, VCAM_VALUE_TYPE,
                                                                    GestureTypeLUT, ARR_SIZE(GestureTypeLUT)
                                                                    );

                for(uint32_t i = 0; i < info->mRegionsNum ; i++ )
                {
                      OMX_STRUCT_INIT(gestInfo.nGestureAreas[i], OMX_CONFIG_OBJECT_RECT_TYPE , mLocalVersion );
                      gestInfo.nGestureAreas[i].eType = (OMX_TI_OBJECT_TYPE)getLutValue( (int)(info->mRegions[i].mObjType), VCAM_VALUE_TYPE,
                                                                                         ObjectTypeLUT, ARR_SIZE(ObjectTypeLUT)
                                                                                        );

                      gestInfo.nGestureAreas[i].nTop = info->mRegions[i].mTop;
                      gestInfo.nGestureAreas[i].nLeft = info->mRegions[i].mLeft;
                      gestInfo.nGestureAreas[i].nWidth = info->mRegions[i].mWidth;
                      gestInfo.nGestureAreas[i].nHeight = info->mRegions[i].mHeight;
                }

                if( OMX_TI_GESTURE_NO_GESTURE != gestInfo.eType)
                {
                    int32_t p = port;
//                    LOOP_PORTS( port , p )
                    {
                        gestInfo.nPortIndex = mCurGreContext.mPortsInUse[p];
                        omxError = OMX_SetConfig( mCurGreContext.mHandleComp,
                                                (OMX_INDEXTYPE)OMX_TI_IndexConfigDetectedGesturesInfo,
                                                &gestInfo
                                              );
                    }
                }
                else
                    greError = -EINVAL;
            }

            break;
        }
#endif
#if ( defined(DUCATI_1_5) || defined(DUCATI_2_0) ) && defined(OMX_CAMERA_SUPPORTS_MANUAL_CONTROLS)
        case VCAM_PARAM_AGC_MIN_DELAY_TIME:
        {
            int32_t delay = *((int32_t*)param);
            OMX_TI_CONFIG_AE_DELAY agcDelTime;
            OMX_STRUCT_INIT( agcDelTime, OMX_TI_CONFIG_AE_DELAY, mLocalVersion );
            agcDelTime.nDelayTime = delay;

            int32_t p = port;
//            LOOP_PORTS( port , p )
            {
                agcDelTime.nPortIndex = p;
                omxError = OMX_SetConfig( mCurGreContext.mHandleComp,
                                        (OMX_INDEXTYPE)OMX_TI_IndexConfigAutoExpMinDelayTime,
                                        &agcDelTime );
            }
            break;
        }

        case VCAM_PARAM_AGC_LOW_TH:
        {
            int32_t lowTH = *((int32_t*)param);
            OMX_TI_CONFIG_AE_THRESHOLD ae;

            OMX_STRUCT_INIT( ae, OMX_TI_CONFIG_AE_THRESHOLD, mLocalVersion );

            int32_t p = port;
//            LOOP_PORTS( port , p )
            {
                ae.nPortIndex = p;
                omxError = OMX_GetConfig( mCurGreContext.mHandleComp, (OMX_INDEXTYPE)OMX_TI_IndexConfigAutoExpThreshold, &ae );

                if( OMX_ErrorNone == omxError )
                {
                    ae.uMinTH = lowTH;
                    omxError = OMX_SetConfig( mCurGreContext.mHandleComp, (OMX_INDEXTYPE)OMX_TI_IndexConfigAutoExpThreshold, &ae );
                }
            }
            break;
        }
        case VCAM_PARAM_AGC_HIGH_TH:
        {
            int32_t highTH = *((int32_t*)param);

            OMX_TI_CONFIG_AE_THRESHOLD ae;
            OMX_STRUCT_INIT( ae, OMX_TI_CONFIG_AE_THRESHOLD, mLocalVersion );

            int32_t p = port;
//            LOOP_PORTS( port , p )
            {
                ae.nPortIndex = p;
                omxError = OMX_GetConfig( mCurGreContext.mHandleComp, (OMX_INDEXTYPE)OMX_TI_IndexConfigAutoExpThreshold, &ae );

                if( OMX_ErrorNone == omxError )
                {
                    ae.uMaxTH = highTH;
                    omxError = OMX_SetConfig( mCurGreContext.mHandleComp, (OMX_INDEXTYPE)OMX_TI_IndexConfigAutoExpThreshold, &ae );
                }
            }
            break;
        }
#endif
        case VCAM_PARAM_NAME:
            // do nothing as this is not supported but it should not fail either
            break;
        default:
        {
            ERROR("Impossible parameter id requested: %d", paramId);
            ERROR("see VisionCamParam_e for possible parameter ids");
            greError = -ENOSYS;
            if(paramId < VCAM_PARAM_MIN || paramId > VCAM_PARAM_MAX)
                greError = -EINVAL;
        }
    }

    if( OMX_ErrorNone != omxError )
    {
        greError = ConvertError(omxError);
    }

    if( greError != 0 )
    {
        ERROR("setParameter() exits with error 0x%x (dec: %d) [OMX:0x%08x] for param id 0x%x",
                greError, greError, omxError, paramId);
    }

    return greError;
}

/*
*  APIs to get configured Vision Cam parameters
*/
int OMXVisionCam::getParameter( VisionCamParam_e paramId, void* param, uint32_t size, VisionCamPort_e port)
{
    int greError = 0;
    OMX_ERRORTYPE omxError = OMX_ErrorNone;

    AutoLock lock(&mUserRequestLock);

    if (param == NULL || size == 0 || port >= VCAM_PORT_MAX)
    {
        ERROR("NULL param pointer passed to %s", __func__);
        return -EINVAL;
    }
    else
    {
        MSG("GET PARAM: 0x%04x, %p, %u (0x%08x)", paramId, param, size, (size==4?*(uint32_t *)param:0));
    }

    if( VCAM_PORT_ALL == port)
    {
        ERROR("%s called on port ALL. Please specifu a port!", __func__);
        return -EINVAL;
    }
    switch( paramId )
    {
        case VCAM_PARAM_HEIGHT:
        {
            OMX_PARAM_PORTDEFINITIONTYPE portCheck;
            omxError = initPortCheck( &portCheck , port );

            if( OMX_ErrorNone == omxError )
            {
                *(int*)param = portCheck.format.video.nFrameHeight;
            }
            break;
        }

        case VCAM_PARAM_WIDTH:
        {
            OMX_PARAM_PORTDEFINITIONTYPE portCheck;
            omxError = initPortCheck( &portCheck , port );

            if( OMX_ErrorNone == omxError )
            {
                *(int*)param = portCheck.format.video.nFrameWidth;
            }
            break;
        }

        case VCAM_PARAM_COLOR_SPACE_FOURCC:
        {
            OMX_PARAM_PORTDEFINITIONTYPE portCheck;
            omxError = initPortCheck(&portCheck, port);

            switch (portCheck.format.video.eColorFormat) {
                case OMX_COLOR_FormatCbYCrY:
                    *((int*)param) = FOURCC('Y','U','Y','V');
                    break;
                case OMX_COLOR_FormatYUV420SemiPlanar:
                    *((int*)param) = FOURCC('N','V','1','2');
                    break;
                default:
                    greError = -EINVAL;
            }
            break;
        }

        case VCAM_PARAM_DO_AUTOFOCUS:
        {
            int i = 0;
            OMX_IMAGE_CONFIG_FOCUSCONTROLTYPE focus;
            OMX_STRUCT_INIT(focus, OMX_IMAGE_CONFIG_FOCUSCONTROLTYPE, mLocalVersion);

            omxError = OMX_GetConfig(mCurGreContext.mHandleComp, OMX_IndexConfigFocusControl, &focus);
            if( OMX_ErrorNone == omxError )
            {
                for( i = 0; i < VCAM_FOCUS_CONTROL_MAX; i++ )
                {
                    if ( FocusModeLUT[ i ][ 1 ] == focus.eFocusControl )
                    {
                        *(int*)param = FocusModeLUT[ i ][ 0 ];
                        break;
                    }
                }
                if( VCAM_FOCUS_CONTROL_MAX == i )
                    greError = -EINVAL;
            }

            break;
        }

        case VCAM_PARAM_DO_MANUALFOCUS:
        {
            OMX_IMAGE_CONFIG_FOCUSCONTROLTYPE focus;
            OMX_STRUCT_INIT(focus, OMX_IMAGE_CONFIG_FOCUSCONTROLTYPE, mLocalVersion);

            omxError = OMX_GetConfig(mCurGreContext.mHandleComp, OMX_IndexConfigFocusControl, &focus);
            if( OMX_ErrorNone == omxError )
            {
                (*(int*)param) = focus.nFocusSteps;
            }
            break;
        }

        case VCAM_PARAM_BRIGHTNESS:
        {
            OMX_CONFIG_BRIGHTNESSTYPE brightness;
            OMX_STRUCT_INIT(brightness, OMX_CONFIG_BRIGHTNESSTYPE, mLocalVersion );

            omxError = OMX_GetConfig( mCurGreContext.mHandleComp, OMX_IndexConfigCommonBrightness, &brightness);
            if( OMX_ErrorNone == omxError )
            {
                *((uint32_t*)param) = brightness.nBrightness;
            }
            break;
        }

        case VCAM_PARAM_CONTRAST:
        {
            OMX_CONFIG_CONTRASTTYPE contrast;
            OMX_STRUCT_INIT(contrast, OMX_CONFIG_CONTRASTTYPE, mLocalVersion );

            omxError = OMX_GetConfig( mCurGreContext.mHandleComp, OMX_IndexConfigCommonContrast, &contrast);
            if( OMX_ErrorNone == omxError )
            {
                *((int*)param) = contrast.nContrast;
            }
            break;
        }

        case VCAM_PARAM_SHARPNESS:
        {
            OMX_IMAGE_CONFIG_PROCESSINGLEVELTYPE procSharpness;
            OMX_STRUCT_INIT( procSharpness, OMX_IMAGE_CONFIG_PROCESSINGLEVELTYPE, mLocalVersion );

            omxError = OMX_GetConfig( mCurGreContext.mHandleComp, (OMX_INDEXTYPE)OMX_IndexConfigSharpeningLevel, &procSharpness);

            if( OMX_ErrorNone == omxError )
            {
                if( OMX_TRUE == procSharpness.bAuto )
                    *((int*)param) = 0;
                else
                    *((int*)param) = procSharpness.nLevel;
            }
            break;
        }

        case VCAM_PARAM_SATURATION:
        {
            OMX_CONFIG_SATURATIONTYPE saturation;
            OMX_STRUCT_INIT(saturation, OMX_CONFIG_SATURATIONTYPE, mLocalVersion);
            omxError = OMX_GetConfig( mCurGreContext.mHandleComp, OMX_IndexConfigCommonSaturation, &saturation);

            if( OMX_ErrorNone == omxError )
            {
                *((int*)param) = saturation.nSaturation;
            }
            break;
        }

        case VCAM_PARAM_FPS_FIXED:
        {
            OMX_PARAM_PORTDEFINITIONTYPE portCheck;
            omxError = initPortCheck( &portCheck , port );

            if( OMX_ErrorNone == omxError )
            {
                *((uint32_t*)param) = portCheck.format.video.xFramerate >> 16;
            }
            break;
        }

        case VCAM_PARAM_FPS_VAR:
        {
            ERROR("No Getting Api for this Parameter %d", paramId);
            break;
        }

        case VCAM_PARAM_FLICKER:
        {
            OMX_CONFIG_FLICKERCANCELTYPE flicker;
            OMX_STRUCT_INIT( flicker, OMX_CONFIG_FLICKERCANCELTYPE, mLocalVersion );
            omxError = OMX_GetConfig( mCurGreContext.mHandleComp, (OMX_INDEXTYPE)OMX_IndexConfigFlickerCancel, &flicker );

            if( OMX_ErrorNone == omxError )
            {
                *((int*)param) = flicker.eFlickerCancel;
            }
            break;
        }

        case VCAM_PARAM_CROP:
        {
            OMX_CONFIG_RECTTYPE crop;
            OMX_STRUCT_INIT(crop, OMX_CONFIG_RECTTYPE, mLocalVersion);

            omxError = OMX_GetConfig( mCurGreContext.mHandleComp, OMX_IndexConfigCommonOutputCrop, &crop );

            if( OMX_ErrorNone == omxError )
            {
                ((VisionCamRectType*)param)->mLeft = crop.nLeft;
                ((VisionCamRectType*)param)->mTop = crop.nTop;
                ((VisionCamRectType*)param)->mWidth = crop.nWidth;
                ((VisionCamRectType*)param)->mHeight = crop.nHeight;
            }
          break;
        }

        case VCAM_PARAM_CAP_MODE:
        {
            if (param != NULL)
            {
                int i = 0;
                OMX_CONFIG_CAMOPERATINGMODETYPE opMode;
                opMode.nSize = sizeof(OMX_CONFIG_CAMOPERATINGMODETYPE);
                memcpy(&opMode .nVersion, mLocalVersion , sizeof(OMX_VERSIONTYPE));

                omxError = OMX_GetParameter( mCurGreContext.mHandleComp, (OMX_INDEXTYPE)OMX_IndexCameraOperatingMode, &opMode );

                if( OMX_ErrorNone == omxError )
                {
                    for (i = 0; i < VCAM_CAP_MODE_MAX; i++)
                    {
                        if (CaptureModeLUT[i][1] == opMode.eCamOperatingMode)
                        {
                            memcpy( param, &CaptureModeLUT[i][0], sizeof(int) );
                            break;
                        }
                    }

                    if( VCAM_CAP_MODE_MAX == CaptureModeLUT[i][0])
                    {
                        greError = -EINVAL;
                        break;
                    }
                }
            }
            break;
        }

        case VCAM_PARAM_SENSOR_SELECT:
        {
            OMX_CONFIG_SENSORSELECTTYPE sensor;
            OMX_STRUCT_INIT(sensor, OMX_CONFIG_SENSORSELECTTYPE, mLocalVersion);

            omxError = OMX_GetConfig( mCurGreContext.mHandleComp, (OMX_INDEXTYPE)OMX_TI_IndexConfigSensorSelect, &sensor);

            if( OMX_ErrorNone == omxError)
            {
                *((int*)param) = sensor.eSensor;
            }
            break;
        }

        case VCAM_PARAM_EXPOSURE_COMPENSATION:
        {
            OMX_CONFIG_EXPOSUREVALUETYPE expValues;
            OMX_STRUCT_INIT( expValues, OMX_CONFIG_EXPOSUREVALUETYPE, mLocalVersion );
            omxError = OMX_GetConfig( mCurGreContext.mHandleComp , OMX_IndexConfigCommonExposureValue , &expValues );

            if( OMX_ErrorNone == omxError )
            {
                expValues.xEVCompensation *= 10;
                int compVal = expValues.xEVCompensation / (1 << 16);
                *((int*)param) = compVal;
            }
            break;
        }

        case VCAM_PARAM_RESOLUTION:
        {
            int i = VCAM_RESOL_MAX;
            unsigned int width, height;
            OMX_PARAM_PORTDEFINITIONTYPE portCheck;
            omxError = initPortCheck( &portCheck , port );

            if( OMX_ErrorNone == omxError )
            {
                width = portCheck.format.video.nFrameWidth;
                height = portCheck.format.video.nFrameHeight;
                for( i = 0; i < VCAM_RESOL_MAX; i++)
                {
                    if( VisionCamResolutions[i].mWidth == width
                        && VisionCamResolutions[i].mHeight == height)
                    {
                        *((int*)param) = i;
                        break;
                    }
                }
                if( VCAM_RESOL_MAX == i )
                {
                    greError = -EINVAL;
                }
            }
            break;
        }

        case VCAM_PARAM_2DBUFFER_DIM:
        {
            OMX_CONFIG_RECTTYPE frame;
            OMX_STRUCT_INIT(frame, OMX_CONFIG_RECTTYPE, mLocalVersion);
            VisionCamResType *pRes = (VisionCamResType *)param;

            frame.nPortIndex = mCurGreContext.mPortsInUse[port];
            if (pRes && size == sizeof(VisionCamResType))
            {
                // Set the port definition to update width and height first.
                omxError = setPortDef( port );

                if( OMX_ErrorNone == omxError )
                {
                    omxError = OMX_GetParameter(mCurGreContext.mHandleComp,
                                                (OMX_INDEXTYPE)OMX_TI_IndexParam2DBufferAllocDimension,
                                                &frame);
                }

                if (OMX_ErrorNone == omxError)
                {
                    pRes->mWidth = frame.nWidth;
                    pRes->mHeight = frame.nHeight;
                }
                else
                {
                    ERROR("Failed to query the 2D Buffer Dimensions! (err=0x%08x)", omxError);
                    memset(pRes, 0, sizeof(VisionCamResType));
                    // omxError will convert to greError
                }
            }
            else
                greError = -EINVAL;
            break;
        }

        case VCAM_PARAM_MANUAL_EXPOSURE:
        {
            OMX_CONFIG_EXPOSUREVALUETYPE expValues;
            OMX_STRUCT_INIT(expValues, OMX_CONFIG_EXPOSUREVALUETYPE, mLocalVersion);

            omxError = OMX_GetConfig( mCurGreContext.mHandleComp , OMX_IndexConfigCommonExposureValue , &expValues );

            if( OMX_ErrorNone == omxError )
            {
                if( OMX_TRUE == expValues.bAutoShutterSpeed )
                {
                    *(OMX_U32*)param = 0;
                }
                else
                {
                    *(OMX_U32*)param = expValues.nShutterSpeedMsec;
                }
            }
            break;
        }

        case VCAM_PARAM_EXPOSURE_ISO:
        {
            OMX_CONFIG_EXPOSUREVALUETYPE expValues;
            OMX_STRUCT_INIT( expValues, OMX_CONFIG_EXPOSUREVALUETYPE, mLocalVersion );

            omxError = OMX_GetConfig( mCurGreContext.mHandleComp , OMX_IndexConfigCommonExposureValue , &expValues );

            if( OMX_ErrorNone == omxError )
            {
                if( OMX_TRUE == expValues.bAutoSensitivity )
                {
                    *(uint32_t*)param = 0;
                }
                else
                    *(uint32_t*)param = expValues.nSensitivity;
            }
            break;
        }

        case VCAM_PARAM_AWB_MODE:
        {
            OMX_CONFIG_WHITEBALCONTROLTYPE wb;
            OMX_STRUCT_INIT(wb, OMX_CONFIG_WHITEBALCONTROLTYPE, mLocalVersion );

            omxError = OMX_GetConfig( mCurGreContext.mHandleComp,
                                 OMX_IndexConfigCommonWhiteBalance,
                                 &wb);

            if( OMX_ErrorNone == omxError )
            {
                *(int *)param = (int)wb.eWhiteBalControl;
            }
            break;
        }

        case VCAM_PARAM_COLOR_TEMP:
        {
            ERROR("No Getting Api for this Parameter %d", paramId);
            break;
        }
        case VCAM_PARAM_WB_COLOR_GAINS:
        {
#ifdef USE_WB_GAIN_PATCH
            OMX_TI_CONFIG_SHAREDBUFFER skipBuffer;

            skipBuffer.nSize = sizeof( OMX_TI_CONFIG_SHAREDBUFFER );
            memcpy( &skipBuffer.nVersion , mLocalVersion , sizeof(mLocalVersion) );
            skipBuffer.pSharedBuff = (OMX_U8*)mWBbuffer;
            skipBuffer.nSharedBuffSize = sizeof(mWBbuffer);

            omxError = OMX_GetConfig( mCurGreContext.mHandleComp,
                                    (OMX_INDEXTYPE) OMX_TI_IndexConfigAAAskipBuffer,
                                    &skipBuffer );

            if( OMX_ErrorNone == omxError )
            {
                VisionCamWhiteBalGains wbGains;
                uint16_t * tmp;
                CALCULATE_WB_GAINS_OFFSET(uint16_t, mWBbuffer, tmp );

                wbGains.mRed = tmp[ RED ];
                wbGains.mGreen_r = tmp[ GREEN_RED ];
                wbGains.mGreen_b = tmp[ GREEN_BLUE ];
                wbGains.mBlue = tmp[ BLUE ];

                *((VisionCamWhiteBalGains*)param) = wbGains;
            }
#endif // USE_WB_GAIN_PATCH
            break;
        }

        case VCAM_PARAM_GAMMA_TBLS:
        {

            VisionCamGammaTableType *gammaTbl = (VisionCamGammaTableType*)param;
            OMX_TI_CONFIG_SHAREDBUFFER skipBuffer;
            skipBuffer.nSize = sizeof( OMX_TI_CONFIG_SHAREDBUFFER );
            memcpy( &skipBuffer.nVersion , mLocalVersion , sizeof(mLocalVersion) );

            skipBuffer.pSharedBuff = (OMX_U8*)mGammaTablesBuf;
            skipBuffer.nSharedBuffSize = sizeof(mGammaTablesBuf);

            omxError = OMX_GetConfig( mCurGreContext.mHandleComp,
                                (OMX_INDEXTYPE) OMX_TI_IndexConfigAAAskipBuffer,
                                &skipBuffer );

            if( OMX_ErrorNone == omxError )
            {
                gammaTbl->mTableSize     = GAMMA_TABLE_SIZE;

                uint32_t* base = (uint32_t *)(mGammaTablesBuf + 12); // 12 bytes offset for red table
                gammaTbl->mRedTable      = (uint16_t*)(base[0] + (uint32_t)&base[2]);
                gammaTbl->mBlueTable     = (uint16_t*)(base[1] + (uint32_t)&base[2]);
                gammaTbl->mGreenTable    = (uint16_t*)(base[2] + (uint32_t)&base[2]);
            }
            break;
        }

        case VCAM_PARAM_ROTATION:
        {
            OMX_CONFIG_ROTATIONTYPE rotation;
            OMX_STRUCT_INIT(rotation, OMX_CONFIG_ROTATIONTYPE, mLocalVersion);
            omxError = OMX_GetConfig(mCurGreContext.mHandleComp,
                                     OMX_IndexConfigCommonRotate,
                                     &rotation);
            if (OMX_ErrorNone == omxError)
                *((OMX_S32*)param) = rotation.nRotation;
            break;
        }

        case VCAM_PARAM_MIRROR:
        {
            if( VCAM_PORT_ALL == port )
            {
                greError = -EINVAL;
            }
            else
            {
                int i;
                OMX_CONFIG_MIRRORTYPE mirror;
                mirror.nSize = sizeof( OMX_CONFIG_MIRRORTYPE );
                mirror.nPortIndex = mCurGreContext.mPortsInUse[port];
                memcpy( &mirror.nVersion, mLocalVersion, sizeof(mLocalVersion));

                omxError = OMX_GetConfig( mCurGreContext.mHandleComp,
                                        OMX_IndexConfigCommonMirror,
                                        &mirror );

                if( OMX_ErrorNone == omxError )
                {
                    for( i = 0; i < VCAM_MIRROR_MAX; i++ )
                    {
                        if( MirrorTypeLUT[i][1] == mirror.eMirror )
                        {
                          *((VisionCamMirrorType*)param) = (VisionCamMirrorType) MirrorTypeLUT[i][0];
                          break;
                        }
                    }

                if( i >= VCAM_MIRROR_MAX )
                    greError = -EINVAL;
                }
            }
            break;
        }

#if ( defined(DUCATI_1_5) || defined(DUCATI_2_0) ) && defined(OMX_CAMERA_SUPPORTS_MANUAL_CONTROLS)
        case VCAM_PARAM_AWB_MIN_DELAY_TIME:
        {
            uint32_t *timeDelay = (uint32_t*)param;
            OMX_TI_CONFIG_AE_DELAY aeDelay;
            OMX_STRUCT_INIT( aeDelay, OMX_TI_CONFIG_AE_DELAY, mLocalVersion );

            omxError = OMX_GetConfig( mCurGreContext.mHandleComp,
                                    ( OMX_INDEXTYPE )OMX_TI_IndexConfigAutoExpMinDelayTime,
                                    &aeDelay
                                  );
            if( OMX_ErrorNone == omxError )
            {
                *timeDelay = aeDelay.nDelayTime;
            }
            break;
        }
#endif
#if ( defined(DUCATI_1_5) || defined(DUCATI_2_0) ) && defined(OMX_CAMERA_SUPPORTS_GESTURES)
        case VCAM_PARAM_GESTURES_INFO:
        {
            VisionCamGestureInfo *info = (VisionCamGestureInfo *)param;
            OMX_TI_CONFIG_GESTURES_INFO gestInfo;
            int ret = 0;

            OMX_STRUCT_INIT( gestInfo, OMX_TI_CONFIG_GESTURES_INFO , mLocalVersion );

            omxError = OMX_GetConfig( mCurGreContext.mHandleComp,
                                    (OMX_INDEXTYPE)OMX_TI_IndexConfigDetectedGesturesInfo,
                                    &gestInfo
                                  );

            if( OMX_ErrorNone != omxError )
                break;

            ret = (VisionCamGestureEvent_e)getLutValue( gestInfo.eType, OMX_VALUE_TYPE,
                                                        GestureTypeLUT, ARR_SIZE(GestureTypeLUT)
                                                        );

            if( -EINVAL == (int)ret )
            {
                greError = -EINVAL;
            }
            else
            {
                info->mGestureType = (VisionCamGestureEvent_e)ret;
                info->mRegionsNum = gestInfo.nNumDetectedGestures;
                if( info->mRegionsNum >= VCAM_Max_Gesture_Per_Frame )
                {
                    greError= -EINVAL;
                    break;
                }

            #ifdef __cplusplus
                info->mRegions = new VisionCamObjectRectType[ info->mRegionsNum ];
            #else
                if( NULL == info->mRegions )
                {
                    ERROR("Please allocate the mRegions buffer");
                    ERROR("Check for necessery size in mRegionsNum");

                    greError= STATUS_NO_RESOURCES;
                    break;
                }
            #endif

                for(uint32_t i = 0; i < info->mRegionsNum; i++ )
                {
                    info->mRegions[i].mObjType= (VisionCamObjectType)getLutValue(
                                                                                    (int)(gestInfo.nGestureAreas[i].eType),
                                                                                    OMX_VALUE_TYPE,
                                                                                    ObjectTypeLUT,
                                                                                    ARR_SIZE(ObjectTypeLUT)
                                                                                );

                    gestInfo.nGestureAreas[i].nTop = info->mRegions[i].mTop;
                    gestInfo.nGestureAreas[i].nLeft = info->mRegions[i].mLeft;
                    gestInfo.nGestureAreas[i].nWidth = info->mRegions[i].mWidth;
                    gestInfo.nGestureAreas[i].nHeight = info->mRegions[i].mHeight;
                }

                info->timeStamp = gestInfo.nTimeStamp;
            }

            break;
        }
#endif
#if ( defined(DUCATI_1_5) || defined(DUCATI_2_0) ) && defined(OMX_CAMERA_SUPPORTS_MANUAL_CONTROLS)
        case VCAM_PARAM_AGC_MIN_DELAY_TIME:
        {
            uint32_t *delay = (uint32_t*)param;
            OMX_TI_CONFIG_AE_DELAY agcDelTime;
            OMX_STRUCT_INIT( agcDelTime, OMX_TI_CONFIG_AE_DELAY, mLocalVersion );

            omxError = OMX_GetConfig( mCurGreContext.mHandleComp,
                                    (OMX_INDEXTYPE)OMX_TI_IndexConfigAutoExpMinDelayTime,
                                    &agcDelTime );

            if( OMX_ErrorNone != omxError )
                break;

            *delay = agcDelTime.nDelayTime;
            break;
        }

        case VCAM_PARAM_AGC_LOW_TH:
        {
            uint32_t *lowTH = (uint32_t*)param;
            OMX_TI_CONFIG_AE_THRESHOLD ae;

            OMX_STRUCT_INIT( ae, OMX_TI_CONFIG_AE_THRESHOLD, mLocalVersion );

            omxError = OMX_GetConfig( mCurGreContext.mHandleComp, (OMX_INDEXTYPE)OMX_TI_IndexConfigAutoExpThreshold, &ae );

            if( OMX_ErrorNone != omxError )
                break;

            *lowTH = ae.uMinTH;
            break;
        }

        case VCAM_PARAM_AGC_HIGH_TH:
        {
            uint32_t *highTH = (uint32_t*)param;

            OMX_TI_CONFIG_AE_THRESHOLD ae;
            OMX_STRUCT_INIT( ae, OMX_TI_CONFIG_AE_THRESHOLD, mLocalVersion );

            omxError = OMX_GetConfig( mCurGreContext.mHandleComp, (OMX_INDEXTYPE)OMX_TI_IndexConfigAutoExpThreshold, &ae );

            if( OMX_ErrorNone != omxError )
                break;

            *highTH = ae.uMaxTH;
            break;
        }
#endif

        default:
        {
            ERROR("Impossible parameter id requested: %d", paramId);
            ERROR("see VisionCamParam_e for possible parameter ids");
            greError = -EINVAL;
        }
    }

    if( OMX_ErrorNone != omxError )
    {
        greError = ConvertError(omxError);
    }

    if( greError != 0 )
    {
        ERROR("getParameter() exits with error 0x%x (dec: %d) for param id %d", greError, greError, paramId);
    }

    return greError;
}

OMX_ERRORTYPE OMXVisionCam::getFocusStatus(OMX_FOCUSSTATUSTYPE *status)
{
    OMX_ERRORTYPE eError = OMX_ErrorNone;
    OMX_PARAM_FOCUSSTATUSTYPE focusStatus;
    OMX_STRUCT_INIT(focusStatus, OMX_PARAM_FOCUSSTATUSTYPE, mLocalVersion);

    // Get the focus status
    OMX_CHECK(eError,OMX_GetConfig(mCurGreContext.mHandleComp,
                                   OMX_IndexConfigCommonFocusStatus,
                                   &focusStatus));
    if (eError == OMX_ErrorNone)
        *status = focusStatus.eFocusStatus;
    return eError;
}

int OMXVisionCam::waitForFocus()
{
    OMX_ERRORTYPE omxError = OMX_ErrorNone;
    OMX_FOCUSSTATUSTYPE focusStatus;

    omxError = RegisterForEvent(mCurGreContext.mHandleComp,
                              (OMX_EVENTTYPE) OMX_EventIndexSettingChanged,
                              OMX_ALL,
                              OMX_IndexConfigCommonFocusStatus,
                              &mGreFocusSem,
                              -1);
    if (OMX_ErrorNone == omxError)
    {
        OMX_CONFIG_CALLBACKREQUESTTYPE focusRequestCallback;
        OMX_STRUCT_INIT(focusRequestCallback, OMX_CONFIG_CALLBACKREQUESTTYPE, mLocalVersion);
        focusRequestCallback.bEnable = OMX_TRUE;
        focusRequestCallback.nIndex = OMX_IndexConfigCommonFocusStatus;

        // subscribe for focus state changes
        OMX_CHECK(omxError,OMX_SetConfig(mCurGreContext.mHandleComp,
                                       (OMX_INDEXTYPE) OMX_IndexConfigCallbackRequest,
                                       &focusRequestCallback));
#ifdef VCAM_CAUTIOUS
        // make sure it's been registered
        OMX_CHECK(omxError,OMX_GetConfig(mCurGreContext.mHandleComp,
                                       (OMX_INDEXTYPE) OMX_IndexConfigCallbackRequest,
                                       &focusRequestCallback));

#endif
        if (OMX_ErrorNone == omxError && focusRequestCallback.bEnable == OMX_TRUE)
        {
            MSG("Waiting for Focus Callback!");
            // wait for focus to arrive
            sem_wait(&mGreFocusSem);

            // Give the client the focus greError
            omxError = getFocusStatus(&focusStatus);
            MSG("Focus Status: %u", focusStatus);
            if (OMX_ErrorNone == omxError)
            {
                if (m_focuscallback)
                    m_focuscallback((int)focusStatus);
            }
        }
    }
    return ConvertError(omxError);
}

int OMXVisionCam::startAutoFocus( VisionCamFocusMode focusMode )
{
    int i;
    OMX_ERRORTYPE omxError = OMX_ErrorNone;
    bool haveEvent = true;
    OMX_IMAGE_CONFIG_FOCUSCONTROLTYPE focus;

    if (focusMode == VCAM_FOCUS_CONTROL_OFF ||
        focusMode == VCAM_FOCUS_CONTROL_AUTO ||
        focusMode == VCAM_FOCUS_CONTROL_CONTINOUS_NORMAL ||
        focusMode == VCAM_FOCUS_CONTROL_CONTINOUS_EXTENDED)
    {
        haveEvent = false;
    }

    // initialize the structure
    OMX_STRUCT_INIT(focus, OMX_IMAGE_CONFIG_FOCUSCONTROLTYPE, mLocalVersion);
    for( i = 0 ; i < VCAM_FOCUS_CONTROL_MAX ; i++ )
    {
        if( FocusModeLUT[ i ][ 0 ] == focusMode )
        {
            focus.eFocusControl = ( OMX_IMAGE_FOCUSCONTROLTYPE )FocusModeLUT[ i ][ 1 ];
            if( focus.eFocusControl == OMX_IMAGE_FocusControlOn )
                focus.nFocusSteps = mManualFocusDistance;
            break;
        }
    }

    if (i == VCAM_FOCUS_CONTROL_MAX)
    {
        ERROR("Unsupported focus mode requested: %d", focusMode );
        return -EINVAL;
    }

    MSG("Focus Requested Steps @ %lu, Index @ %lu", focus.nFocusSteps, focus.nFocusStepIndex);

    // tell the OMX component to change the focus control mode
    OMX_CHECK(omxError,OMX_SetConfig(mCurGreContext.mHandleComp, OMX_IndexConfigFocusControl, &focus));
#ifdef VCAM_CAUTIOUS
    // ask it what mode it's in now
    OMX_CHECK(omxError, OMX_GetConfig(mCurGreContext.mHandleComp, OMX_IndexConfigFocusControl, &focus));
#endif
    MSG("Focus Control Mode = 0x%08x", focus.eFocusControl);
    if (OMX_ErrorNone == omxError && focus.eFocusControl != OMX_IMAGE_FocusControlOff)
    {
        if (haveEvent)
        {
            pthread_t thread;
            pthread_create(&thread, NULL, FocusThreadLauncher, this);
            // the thread will die by itself, we don't need to join it.
        }
    }

    return ConvertError(omxError);
}

/* This will set the preview buffer sizzes, format, etc.
    */
OMX_ERRORTYPE OMXVisionCam::setPortDef( int port )
{
    OMX_ERRORTYPE omxError = OMX_ErrorNone;

        VCAM_PortParameters * portData = NULL;
        OMX_PARAM_PORTDEFINITIONTYPE portCheck;

        int32_t p;
        LOOP_PORTS( port , p )
        {
            omxError = initPortCheck(&portCheck, p);

            if( omxError == OMX_ErrorNone )
            {
                portData =  &mCurGreContext.mCameraPortParams[p];
                portCheck.format.video.nFrameWidth  = portData->mWidth;
                portCheck.format.video.nFrameHeight = portData->mHeight;
                portCheck.format.video.eColorFormat = portData->mColorFormat;
                portCheck.format.video.nStride      = portData->mStride;
                portCheck.format.video.xFramerate   = portData->mFrameRate << 16;
                portCheck.nBufferCountActual        = portData->mNumBufs;

                omxError = OMX_SetParameter(mCurGreContext.mHandleComp,
                                          OMX_IndexParamPortDefinition,
                                          &portCheck);
            }

            if( omxError == OMX_ErrorNone )
                omxError = initPortCheck(&portCheck, p);

            if( omxError == OMX_ErrorNone )
                portData->mBufSize = portCheck.nBufferSize;

#if defined(VCAM_SET_FORMAT_ROTATION)
// set the rotation type on the port
if ( omxError == OMX_ErrorNone)
{
    OMX_CONFIG_ROTATIONTYPE rotType;
    OMX_STRUCT_INIT(rotType, OMX_CONFIG_ROTATIONTYPE, mLocalVersion);
    rotType.nRotation = mCurGreContext.mCameraPortParams[p].mRotation;
    rotType.nPortIndex = mCurGreContext.mPortsInUse[p];
    MSG("VCAM: Configuring for Rotation %li",rotType.nRotation);
    OMX_CHECK(omxError, OMX_SetConfig(mCurGreContext.mHandleComp,
                                      OMX_IndexConfigCommonRotate,
                                      &rotType));
}
#endif
        }

    return omxError;
}

/* This will start the preview.
 * Before that setParameter() sould be called, to configure the preview port.
 * This method is only called internal for OMXVisionCam
 * by sendCommand().
 */
int OMXVisionCam::startPreview( VisionCamPort_e port )
{
    int greError = 0;

    if( port < VCAM_PORT_ALL || port > VCAM_PORT_MAX - 1 )
    {
        ERROR("startPreview() called, but port is not specified properly.");
        return -EINVAL;
    }

    mFlushInProcess = false;

    switch((int)getComponentState())
    {
        case OMX_StateIdle:
        {
            greError = transitToState( OMX_StateExecuting );
            mReturnToExecuting = true;
            break;
        }
        case OMX_StateLoaded:
        case OMX_StateWaitForResources:
        case OMX_StateInvalid:
        {
            ERROR("Calling startPreview() in an inproper state.");
            greError = -EBADE;
            break;
        }
        case OMX_StateExecuting:
        case OMX_StatePause:
        {
            break;
        }
        default:
            break;
    }

    AutoLock lock(&mFrameBufferLock);

    OMX_ERRORTYPE omxError;
    omxError = setPortDef( port );
    greError = ConvertError(omxError);

    if( 0 == greError )
    {
        greError = portEnableDisable( OMX_CommandPortEnable, populatePortSrvc, port);
    }

    if( 0 == greError )
    {
        // first fill the video port buffers, then preview port
        // otherwise preview port will start working right after it receive its frames
        // so it won't wait for video port to get ready
        if( VCAM_PORT_ALL == port || VCAM_PORT_VIDEO == port )
        {
            greError = fillPortBuffers(VCAM_PORT_VIDEO);
        }

        if( VCAM_PORT_ALL == port || VCAM_PORT_PREVIEW == port )
        {
            greError = fillPortBuffers(VCAM_PORT_PREVIEW);
        }
    }

    return greError;
}

/**
  * This will stop the preview
  *
  */
int OMXVisionCam::stopPreview( VisionCamPort_e port )
{
    int greError = 0;
    bool goToIdle = true;

    mFlushInProcess = true;

    flushBuffers( port );
    greError = portEnableDisable( OMX_CommandPortDisable, freePortBuffersSrvc, port );

    if( 0 == greError )
    {
        for( int p = VCAM_PORT_PREVIEW; p < VCAM_PORT_MAX; p++)
        {
            if( mCurGreContext.mCameraPortParams[p].mIsActive )
            {
                goToIdle = false;
                break;
            }
        }

        if ( goToIdle && OMX_StateExecuting == getComponentState() )
        {
            transitToState( OMX_StateIdle );
        }
    }

    return greError;
}

/**
  * This will free all the buffers on the preview port
  * and switch to loaded state.
  */
int OMXVisionCam::releaseBuffers( VisionCamPort_e port)
{
    int greError = 0;

    OMX_PARAM_PORTDEFINITIONTYPE portCheck[ VCAM_PORT_MAX ];

    int32_t p;
    LOOP_PORTS( port , p )
    {
        initPortCheck(&portCheck[p], p);

        for( uint32_t indBuff = 0; indBuff < portCheck[p].nBufferCountActual; indBuff++ )
        {
            if( mFrameDescriptors && mFrameDescriptors[p] && mFrameDescriptors[p][indBuff] )
                delete mFrameDescriptors[p][indBuff];
        }

        if( mFrameDescriptors && mFrameDescriptors[p] )
        {
            delete mFrameDescriptors[p];
            mFrameDescriptors[p] = NULL;
        }
    }

    if (OMX_StateIdle == getComponentState())
        transitToState( OMX_StateLoaded, NULL, NULL );

    return greError;
}

/** This will configure the component to start/stop face detection.
 * Will also start/stop face detection extra data
 * faces coordinates will be written into camera frame received
 * in preview callback
 */
int OMXVisionCam::enableFaceDetect(VisionCamPort_e port)
{
    OMX_ERRORTYPE omxError = OMX_ErrorNone;
    OMX_CONFIG_OBJDETECTIONTYPE objDetection;
    OMX_STRUCT_INIT( objDetection, OMX_CONFIG_OBJDETECTIONTYPE ,mLocalVersion);
    objDetection.nPortIndex = mCurGreContext.mPortsInUse[port];

    if (mFaceDetectionEnabled)
        objDetection.bEnable = OMX_TRUE;
    else
        objDetection.bEnable = OMX_FALSE;

    omxError = OMX_SetConfig(mCurGreContext.mHandleComp, (OMX_INDEXTYPE) OMX_IndexConfigImageFaceDetection, &objDetection);
    if( OMX_ErrorNone == omxError )
    {
        OMX_CONFIG_EXTRADATATYPE xData;
        OMX_STRUCT_INIT(xData, OMX_CONFIG_EXTRADATATYPE, mLocalVersion);

        xData.nPortIndex        = mCurGreContext.mPortsInUse[port];
        xData.eExtraDataType    = OMX_FaceDetection;
#if defined(TUNA) || defined(MAGURO)
        xData.eCameraView       = OMX_2D_Prv;
#endif
        if (mFaceDetectionEnabled)
            xData.bEnable = OMX_TRUE;
        else
            xData.bEnable = OMX_FALSE;

        OMX_CHECK(omxError,OMX_SetConfig(mCurGreContext.mHandleComp, (OMX_INDEXTYPE)OMX_IndexConfigOtherExtraDataControl, &xData));
    }
    return ConvertError(omxError);
}

/** Fill the face coordinates field in camera frame,
 *  which will be received by OMXVisionCam client at each frame.
 */
void OMXVisionCam::getFacesCoordinates( VisionCamFrame *frame)
{
    OMX_OTHER_EXTRADATATYPE* extraData = (OMX_OTHER_EXTRADATATYPE*)frame->mExtraDataBuf;
    OMX_U8 *pExtraLimit = (OMX_U8 *)extraData + frame->mExtraDataLength;
    OMX_FACEDETECTIONTYPE *facesData;
    if( extraData == NULL )
    {
        frame->mDetectedFacesNum = 0;
        return;
    }

    // find the faces data buffers
    while( (OMX_EXTRADATATYPE)OMX_FaceDetection != extraData->eType && extraData->data )
    {
        MSG("Current Extra Data Section Size: %lu", extraData->nDataSize);
        extraData = (OMX_OTHER_EXTRADATATYPE*)(extraData->data + extraData->nDataSize);

        if( (OMX_U8 *)extraData >= pExtraLimit )
        {
            ERROR("ERROR: METADATA: Bad size field in metadata. %p >= %p", extraData, pExtraLimit);
            return;
        }

        if( 0 == extraData->nDataSize )
        {
            ERROR("METADATA: No face data detected!");
            frame->mDetectedFacesNum = 0;
            memset( frame->mFaces, 0, MAX_FACES_COUNT*sizeof(VisionCamFaceType) );
            return;
        }
    }

    if( extraData->data )
    {
        facesData = (OMX_FACEDETECTIONTYPE *)(extraData->data);

        frame->mDetectedFacesNum = facesData->ulFaceCount;

        MSG("METADATA: FACE # %d!", frame->mDetectedFacesNum);

        for(uint32_t i = 0; i < frame->mDetectedFacesNum; i++)
        {
            memcpy( &(frame->mFaces[i]), &(facesData->tFacePosition[i].nScore), sizeof(VisionCamFaceType)) ;
        }
    }
}

/** Parse through the extra data type structure to find the pointer to the relevent
 *  data type.  This is to abstract the port number, version, and packet structure from the client.
 */
void OMXVisionCam::getMetadataPtrs( VisionCamFrame *frame)
{
    OMX_OTHER_EXTRADATATYPE* extraData = (OMX_OTHER_EXTRADATATYPE*)frame->mExtraDataBuf;
    OMX_U8 *pExtraLimit = (OMX_U8 *)extraData + frame->mExtraDataLength;
    frame->mMetadata.mAutoWBGains = NULL;
    frame->mMetadata.mManualWBGains = NULL;
    frame->mMetadata.mAncillary = NULL;
    frame->mMetadata.mHistogram2D = NULL;
    frame->mMetadata.mHistogramL = NULL;
    frame->mMetadata.mHistogramR = NULL;

    if( extraData == NULL )
    {
        return;
    }

    while( extraData->eType && extraData->nDataSize && extraData->data )   // keep looping while there is more extra data and double check size
    {
        switch( extraData->eType )
        {
            case OMX_AncillaryData:
            {
                OMX_TI_ANCILLARYDATATYPE *ancillaryData = NULL;
                MSG("METADATA: Found Ancillary Data Section!");
                ancillaryData =  (OMX_TI_ANCILLARYDATATYPE  *)(extraData->data);
                frame->mMetadata.mAncillary= (VisionCamAncillary*)&ancillaryData->nAncillaryDataVersion;
                break;
            }

            case OMX_WhiteBalance:
            {
                OMX_TI_WHITEBALANCERESULTTYPE *wbData = NULL;
                MSG("METADATA: Found White Balance Result Data Section!");
                wbData =  (OMX_TI_WHITEBALANCERESULTTYPE  *)(extraData->data);
                frame->mMetadata.mAutoWBGains = (VisionCamWhiteBalGains*)&wbData->nGainR;
                break;
            }
#ifndef __QNX__
            case OMX_TI_WhiteBalanceOverWrite:
            {
                OMX_TI_WHITEBALANCERESULTTYPE *wbManData = NULL;
                MSG("METADATA: Found White Balance Overwrite Data Section!");
                wbManData =  (OMX_TI_WHITEBALANCERESULTTYPE  *)(extraData->data);
                frame->mMetadata.mManualWBGains = (VisionCamWhiteBalGains*)&wbManData->nGainR;
                break;
            }
#endif
#if defined(DUCATI_2_0)
            case OMX_Histogram:
            {
                OMX_TI_HISTOGRAMTYPE *histData = NULL;
                MSG("METADATA: Found Histogram Result Data Section!");
                histData =  (OMX_TI_HISTOGRAMTYPE  *)(extraData->data);
#if defined(TUNA) || defined(MAGURO) || 1
                if((OMX_TI_CAMERAVIEWTYPE)(histData->eCameraView) == OMX_2D_Prv)
                    frame->mMetadata.mHistogram2D = (VisionCamHistogram*)&histData->nBins;
                else if((OMX_TI_CAMERAVIEWTYPE)(histData->eCameraView) == OMX_3D_Left_Prv)
                    frame->mMetadata.mHistogramL = (VisionCamHistogram*)&histData->nBins;
                else if((OMX_TI_CAMERAVIEWTYPE)(histData->eCameraView) == OMX_3D_Right_Prv)
                    frame->mMetadata.mHistogramR = (VisionCamHistogram*)&histData->nBins;
#else
                frame->mMetadata.mHistogram2D = (VisionCamHistogram*)&histData->nBins;
#endif
                break;
            }
#endif
            default:
            {
                break;
            }
        }
        MSG("Current Extra Data Section Size: %lu", extraData->nDataSize);
        extraData = (OMX_OTHER_EXTRADATATYPE*)(extraData->data + extraData->nDataSize);
        if( (OMX_U8 *)extraData >= pExtraLimit )
        {
            ERROR("ERROR: METADATA: Bad size field in metadata. %p >= %p", extraData, pExtraLimit);
            break;
        }
    }
}

/** This shall be called by the user of this class when it finishes working
 * with the frame and to notify VisionCam that the frame buffer is
 * free.
 */
int OMXVisionCam::returnFrame(VisionCamFrame *cameraFrame)
{
    OMX_ERRORTYPE omxError = OMX_ErrorNone;
    int32_t i = 0;
    if (mFlushInProcess)
        return 0;

    if (cameraFrame == NULL)
        return -EINVAL;

    if (OMX_StateExecuting == getComponentState())
    {
        VisionCamPort_e port = cameraFrame->mFrameSource;
        VCAM_PortParameters * portData = &mCurGreContext.mCameraPortParams[port];
        for (i = 0; i < portData->mNumBufs; i++)
        {
            if (portData->mBufferHeader[i] &&
                portData->mBufferHeader[i]->pAppPrivate == cameraFrame->mFrameBuff)
            {
                omxError = OMX_FillThisBuffer(mCurGreContext.mHandleComp,
                                              mCurGreContext.mCameraPortParams[port].mBufferHeader[i]);
                if (OMX_ErrorNone != omxError)
                {
                    ERROR("ERROR: OMX_FillThisBuffer() returned 0x%x in %s", omxError, __func__);
                }
                else
                {
                    MSG("frame returned successfully in %s", __func__);
                }
                break;
            }
        }
        if( i == portData->mNumBufs)
        {
            ERROR("ERROR: returned frame not found in %s", __func__);
        }
    }
    else
    {
        ERROR("Returning Frame when OMX-CAMERA is in the wrong state or image is NULL");
    }

    return ConvertError(omxError);
}

int OMXVisionCam::PreemptionService(/*VisionCamPreemptionActivity_e activity*/)
{
    OMX_ERRORTYPE omxError = OMX_ErrorNone;
    VCAM_PortParameters * portData = NULL;

    switch( mPreemptionState )
    {
        case VCAM_PREEMPT_SUSPEND:
        {
            // Register for Loaded state switch event
            omxError = RegisterForEvent(mCurGreContext.mHandleComp,
                                         OMX_EventCmdComplete,
                                         OMX_CommandStateSet,
                                         OMX_StateLoaded,
                                         &mGreLocalSem,
                                         -1); // Infinite timeout

            if( OMX_ErrorNone == omxError )
            {
                for( int port = VCAM_PORT_PREVIEW; port < VCAM_PORT_MAX; port++ )
                {
                    // Free the OMX Buffers
                    portData = &mCurGreContext.mCameraPortParams[port];
                    for( int i = 0; i < portData->mNumBufs; i++ )
                    {
                        omxError = OMX_FreeBuffer(mCurGreContext.mHandleComp,
                                                        mCurGreContext.mPortsInUse[port],
                                                        portData->mBufferHeader[i]);

                        portData->mBufferHeader[i] = NULL;

                        if( OMX_ErrorNone != omxError )
                        {
                            ERROR("Preemption Service: Error 0x%x while freeing buffers!", omxError);
                            break;
                        }
                    }
                }
            }

            if( OMX_ErrorNone == omxError )
                sem_wait(&mGreLocalSem);

            if( mClientNotifier.mNotificationCallback && OMX_ErrorNone == omxError )
                mClientNotifier.mNotificationCallback(VisionCamClientNotifier::VCAM_MESSAGE_PREEMPT_SUSPEND_ACTIVITY);

            // Register for WAIT state switch event
            omxError = RegisterForEvent(mCurGreContext.mHandleComp,
                                         OMX_EventCmdComplete,
                                         OMX_CommandStateSet,
                                         OMX_StateWaitForResources,
                                         &mGreLocalSem,
                                         -1); // Infinite timeout

            if( OMX_ErrorNone == omxError  )
            {
              omxError = OMX_SendCommand(mCurGreContext.mHandleComp,
                                            OMX_CommandStateSet,
                                            OMX_StateWaitForResources,
                                            NULL);
            }

            if( OMX_ErrorNone == omxError  )
              sem_wait(&mGreLocalSem);

            break;
        }
        case VCAM_PREEMPT_RESUME:
        {
            if( mClientNotifier.mNotificationCallback )
                mClientNotifier.mNotificationCallback(VisionCamClientNotifier::VCAM_MESSAGE_PREEMPT_RESUME_ACTIVITY);

            // Register for IDLE state switch event
            omxError = RegisterForEvent(mCurGreContext.mHandleComp,
                                           OMX_EventCmdComplete,
                                           OMX_CommandStateSet,
                                           OMX_StateIdle,
                                           &mGreLocalSem,
                                           -1); //Infinite timeout
            if( OMX_ErrorNone != omxError )
            {
                ERROR("Preemption Service: Error 0x%x while registering for Idle state wait.", omxError);
                break;
            }

            for( int port = VCAM_PORT_PREVIEW; port < VCAM_PORT_MAX; port++ )
            {
                portData = &mCurGreContext.mCameraPortParams[port];
                for( int buff = 0; buff < portData->mNumBufs; buff++ )
                {
                    OMX_BUFFERHEADERTYPE *pBufferHdr;
                    OMX_U8 *buffer = (OMX_U8*)mBuffersInUse[port].mBuffers[buff].fd;

                    omxError = OMX_UseBuffer(   mCurGreContext.mHandleComp,
                                                &pBufferHdr,
                                                mCurGreContext.mPortsInUse[port],
                                                0,
                                                portData->mBufSize,
                                                buffer
                                            );

                    if( OMX_ErrorNone != omxError )
                    {
                        ERROR("Preemption Service: Error 0x%x while passing buffers to OMX Component.", omxError);
                        break;
                    }

                    pBufferHdr->pAppPrivate = (OMX_PTR)&mBuffersInUse[port].mBuffers[buff];

                    pBufferHdr->nSize = sizeof(OMX_BUFFERHEADERTYPE);

                    memcpy( &(pBufferHdr->nVersion), mLocalVersion, sizeof( OMX_VERSIONTYPE ) );

                    portData->mBufferHeader[buff] = pBufferHdr;
                }
            }

            sem_wait(&mGreLocalSem);

//            if (mReturnToExecuting)
//            {
                for( int port = VCAM_PORT_PREVIEW; port < VCAM_PORT_MAX; port++ )
                {
                    if( mCurGreContext.mCameraPortParams[port].mIsActive )
                        startPreview( (VisionCamPort_e)port );
                }
//            }
            break;
        }

        case VCAM_PREEMPT_WAIT_TO_START:
        {
            if( mClientNotifier.mNotificationCallback && OMX_ErrorNone == omxError )
                mClientNotifier.mNotificationCallback(VisionCamClientNotifier::VCAM_MESSAGE_PREEMPT_WAIT_RESOURCES);

            // Register for WAIT state switch event
            omxError = RegisterForEvent(mCurGreContext.mHandleComp,
                                           OMX_EventCmdComplete,
                                           OMX_CommandStateSet,
                                           OMX_StateWaitForResources,
                                           &mGreLocalSem,
                                           -1); // Infinite timeout

           if( OMX_ErrorNone == omxError  )
           {
               omxError = OMX_SendCommand(mCurGreContext.mHandleComp,
                                             OMX_CommandStateSet,
                                             OMX_StateWaitForResources,
                                             NULL);
           }

           if( OMX_ErrorNone == omxError  )
               sem_wait(&mGreLocalSem);

           // Register for WAIT state switch event
           omxError = RegisterForEvent(mCurGreContext.mHandleComp,
                                          OMX_EventResourcesAcquired,
                                          0,
                                          0,
                                          &mGreLocalSem,
                                          -1); // Infinite timeout

           if( OMX_ErrorNone == omxError  )
               sem_wait(&mGreLocalSem);

           if( mClientNotifier.mNotificationCallback && OMX_ErrorNone == omxError )
               mClientNotifier.mNotificationCallback(VisionCamClientNotifier::VCAM_MESSAGE_PREAMPT_REASOURCES_READY);

            break;
        }
        default:

            break;
    }

    if( OMX_ErrorNone == omxError  )
        mPreemptionState = VCAM_PREEMPT_INACTIVE;

    return ConvertError(omxError);
}

OMX_ERRORTYPE OMXVisionCam::RegisterForEvent(OMX_IN OMX_HANDLETYPE hComponent,
                                             OMX_IN OMX_EVENTTYPE eEvent,
                                             OMX_IN OMX_U32 nData1,
                                             OMX_IN OMX_U32 nData2,
                                             OMX_IN sem_t *semaphore,
                                             OMX_IN OMX_U32 timeout)
{
    OMXVCAM_Msg_t *msg = (OMXVCAM_Msg_t *)calloc(1, sizeof(OMXVCAM_Msg_t));
    OMX_ERRORTYPE err = OMX_ErrorNone;
    if (msg)
    {
        msg->eEvent = eEvent;
        msg->nData1 = nData1;
        msg->nData2 = nData2;
        msg->semaphore = semaphore;
        msg->hComponent = hComponent;
        msg->timeout = timeout;
        mEventSignalQ.push_back(msg);
        MSG("Registering for Event %d (0x%08x)", eEvent,eEvent);
    }
    else
    {
        err = OMX_ErrorInsufficientResources;
    }
    return err;

}

extern "C" int node_compare_vcam_msg(void* a, void* b)
{
    OMXVCAM_Msg_t *msgA = (OMXVCAM_Msg_t *)a;
    OMXVCAM_Msg_t *msgB = (OMXVCAM_Msg_t *)b;

    MSG("Comparing Node %p and Node %p",msgA, msgB);

    if (msgA->eEvent == msgB->eEvent &&
        msgA->nData1 == msgB->nData1 &&
        msgA->nData2 == msgB->nData2 &&
        msgA->hComponent == msgB->hComponent)
        return 0;
    else if (msgA->eEvent < msgB->eEvent)
        return -1;
    else // if (msgA->event > msgB->event)
        return 1;
}

/** OMXVisionCam Event Handler from OMX-CAMERA */
OMX_ERRORTYPE OMXVisionCam::EventHandler(OMX_IN OMX_HANDLETYPE hComponent,
                                         OMX_IN OMX_PTR pAppData,
                                         OMX_IN OMX_EVENTTYPE eEvent,
                                         OMX_IN OMX_U32 nData1,
                                         OMX_IN OMX_U32 nData2,
                                         OMX_IN OMX_PTR pEventData)
{
    // make a local pointer to the instance class which registered this callback.
    OMXVisionCam *pOMXCam = (OMXVisionCam*)pAppData;
    size_t len = pOMXCam->mEventSignalQ.size();

    MSG("OMXVisionCam::EventHandler() event=%d arg1=%08x arg2=%08x ptr=%p (len=%zu)", eEvent, (uint32_t)nData1, (uint32_t)nData2, pEventData, len);
    // check the queue to see if we were waiting on an event
    if (len > 0)
    {
        OMXVCAM_Msg_t* msg = NULL;
        std::list<OMXVCAM_Msg_t*>::iterator it;
        for (it = pOMXCam->mEventSignalQ.begin(); it != pOMXCam->mEventSignalQ.end(); ++it) {
          if ((*it)->eEvent > eEvent) {
            break;
          }
          if (((*it)->eEvent == eEvent) &&
              ((*it)->nData1 == nData1) &&
              ((*it)->nData2 == nData2) &&
              ((*it)->hComponent == hComponent)) {
            msg = *it;
            break;
          }
        }

        // find the list event which matches this event
        if (msg != NULL)
        {
            MSG("Found Event in List which matches incoming event!");
            if (msg->semaphore)
                sem_post(msg->semaphore);

            pOMXCam->mEventSignalQ.erase(it);
            free(msg);
        }
        else
        {
            MSG("No matching event found in list!");
        }
    }

    switch( eEvent )
    {
        case OMX_EventError:
        {
            switch( nData1 )
            {
                case OMX_ErrorResourcesPreempted:
                    break;
                case OMX_ErrorResourcesLost:
                {
                    pthread_t thread;
                    ERROR("OMXVisionCam lost resources!");
                    pOMXCam->mPreemptionState = VCAM_PREEMPT_SUSPEND;
                    pthread_create(&thread, NULL, PreemptionThreadLauncher, pOMXCam);
                    break;
                }
                case OMX_ErrorInsufficientResources:
                {
                    pthread_t thread;
                    ERROR("OMXVisionCam has insufficient resources!");
                    pOMXCam->mPreemptionState = VCAM_PREEMPT_SUSPEND;
                    pthread_create(&thread, NULL, PreemptionThreadLauncher, pOMXCam);
                    break;
                }
                case OMX_ErrorIncorrectStateOperation:
                {
                    break;
                }
                case OMX_ErrorInvalidState:
                {
                    break;
                }
                default:

                    break;
            }
            break;
        }
        case OMX_EventResourcesAcquired:
        {
            break;
        }
        case OMX_EventCmdComplete:
        {
            switch (nData1)
            {
                case OMX_CommandStateSet:
                    PrintOMXState((OMX_STATETYPE)nData2);
                    break;
                case OMX_CommandFlush:
                case OMX_CommandPortDisable:
                case OMX_CommandPortEnable:
                    break;
            }
            break;
        }
        case OMX_EventPortSettingsChanged:
        case OMX_EventIndexSettingChanged:
            MSG("*** Port/Index Settings have Changed!");
            break;
        default:
          break;
    }
    return OMX_ErrorNone;
}

//GRE Empty buffer done callback
OMX_ERRORTYPE OMXVisionCam::EmptyBufferDone(OMX_IN OMX_HANDLETYPE hComponent,
                                             OMX_IN OMX_PTR pAppData,
                                             OMX_IN OMX_BUFFERHEADERTYPE* pBuffHeader)
{
//     OMXVisionCam *pOMXCam = (OMXVisionCam *)pAppData;
    pAppData = pAppData;
    hComponent = hComponent;
    pBuffHeader = pBuffHeader;
    return OMX_ErrorNotImplemented;
}

//GRE fill buffer done callback
OMX_ERRORTYPE OMXVisionCam::FillBufferDone(OMX_IN OMX_HANDLETYPE hComponent,
                                            OMX_IN OMX_PTR pAppData,
                                            OMX_IN OMX_BUFFERHEADERTYPE* pBuffHeader) {
    OMXVisionCam* pCam = static_cast<OMXVisionCam*>(pAppData);
    MSG("OMX-CAMERA has returned a frame (%p) offset=%lu!", pBuffHeader->pAppPrivate, pBuffHeader->nOffset);
    pCam->mFrameCbData.frames.push_back(pBuffHeader);
    pthread_mutex_unlock(&pCam->mFrameCbData.mutex);
    return OMX_ErrorNone;
}

void OMXVisionCam::frameReceivedSrvc(void *data)
{
    OMX_BUFFERHEADERTYPE* pBuffHeader = (OMX_BUFFERHEADERTYPE* )data;
    VisionCamFrame *cFrame = NULL;
    VisionCamPort_e portInd = VCAM_PORT_MAX;

    if( !data )
    {
        return;
    }

    if( mFlushInProcess )
    {
        return;
    }

    AutoLock lock(&mFrameBufferLock);

    switch(pBuffHeader->nOutputPortIndex)
    {
        case VCAM_CAMERA_PORT_VIDEO_OUT_PREVIEW:
        {
            portInd = VCAM_PORT_PREVIEW;
            break;
        }
        case VCAM_CAMERA_PORT_VIDEO_OUT_VIDEO:
        {
            portInd = VCAM_PORT_VIDEO;
            break;
        }
        default:
        {
            portInd = VCAM_PORT_MAX;
            break;
        }
    }

    if( portInd > VCAM_PORT_ALL && portInd < VCAM_PORT_MAX && mCurGreContext.mCameraPortParams[portInd].mIsActive )
    {
        // find the frame descriptor corresponding to received buffer
        for( int i = 0; i < mCurGreContext.mCameraPortParams[portInd].mNumBufs; i++ )
        {
            if( mFrameDescriptors[portInd][i]->mFrameBuff == pBuffHeader->pAppPrivate )
            {
                cFrame = mFrameDescriptors[portInd][i];
                break;
            }
        }
    }

    if( cFrame )
    {
        cFrame->mFrameSource     = portInd;
        cFrame->mLength          = pBuffHeader->nFilledLen;
        cFrame->mTimestamp       = pBuffHeader->nTimeStamp;
        cFrame->mWidth           = mCurGreContext.mCameraPortParams[portInd].mWidth;
        cFrame->mHeight          = mCurGreContext.mCameraPortParams[portInd].mHeight;
        cFrame->mCookie          = m_cookie;
        cFrame->mContext         = this;
        cFrame->mMetadata.mAutoWBGains = NULL;
        cFrame->mMetadata.mManualWBGains = NULL;
        cFrame->mMetadata.mAncillary = NULL;
        cFrame->mMetadata.mHistogram2D = NULL;
        cFrame->mMetadata.mHistogramL = NULL;
        cFrame->mMetadata.mHistogramR = NULL;
        cFrame->mExtraDataBuf    = NULL;
        cFrame->mExtraDataLength = 0;
        cFrame->mDetectedFacesNum = 0;
        memset( cFrame->mFaces, 0, MAX_FACES_COUNT * sizeof(VisionCamFaceType) );

        if( VCAM_PORT_PREVIEW == cFrame->mFrameSource )
            m_frameNum++;

        if (m_callback != NULL)
        {
            m_callback(cFrame);
        }
    }
    else
    {
        ERROR("ERROR: a frame buffer is received, but there is no matching buffer." );
    }
}

#if TIME_PROFILE
void OMXVisionCam::PopulateTimeProfiler()
{
    mTimeProfiler[ first ] = new VisionCamTimePtofile("first");
    mTimeProfiler[ second ] = new VisionCamTimePtofile();
    mTimeProfiler[ last ] = new VisionCamTimePtofile();
}

VisionCamTimePtofile::VisionCamTimePtofile( const char * name ){
  memset( &mStart, 0, sizeof(struct timeval) );
  memset( &mEnd, 0, sizeof(struct timeval) );

  if( name )
    mName = name;
  else
    mName = "unknown";
};

VisionCamTimePtofile::~VisionCamTimePtofile(){
    dump();
};

void VisionCamTimePtofile::dump() {
    if( mEnd.tv_usec && mStart.tv_usec )
    {
        double time = (double)(mEnd.tv_usec = mStart.tv_usec) / (double)1000;
        printf("TIME: %s - %g [ms]\r", mName, time );
        memset( &mStart, 0, sizeof(struct timeval) );
        memset( &mEnd, 0, sizeof(struct timeval) );
    }
}

#endif // TIME_PROFILE
