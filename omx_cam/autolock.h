/*
 *  Copyright (C) 2009-2011 Texas Instruments, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef _AUTOLOCK_H
#define _AUTOLOCK_H

#include <pthread.h>

class AutoLock {
  pthread_mutex_t* m_;
public:
  AutoLock(pthread_mutex_t* m): m_(m) {
    pthread_mutex_lock(m_);
  }
  ~AutoLock() {
    pthread_mutex_unlock(m_);
  }
};

#endif // _AUTOLOCK_H
