/*
 *  Copyright (c) 2013-2014, Texas Instruments Incorporated
 *  Author: alaganraj <alaganraj.s@ti.com>
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *  *  Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *  *  Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *
 *  *  Neither the name of Texas Instruments Incorporated nor the names of
 *     its contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 *  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 *  THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 *  PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 *  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 *  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 *  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 *  OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 *  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 *  OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 *  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  Contact information for paper mail:
 *  Texas Instruments
 *  Post Office Box 655303
 *  Dallas, Texas 75265
 *  Contact information:
 *  http://www-k.ext.ti.com/sc/technical-support/product-information-centers.htm?
 *  DCMP=TIHomeTracking&HQS=Other+OT+home_d_contact
 *  ============================================================================
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdint.h>
#include <string.h>
#include <errno.h>

#include <linux/videodev2.h>
#include <linux/v4l2-controls.h>

#include <sys/mman.h>
#include <sys/ioctl.h>

#include <xf86drm.h>
#include <omap_drm.h>
#include <omap_drmif.h>

#include "util.h"

#include "vpe-common.c"

#define NUMBUF	6 //to be removed

/** Input  file descriptor */
static int fin = -1;

/** OMAPDRM buffer objects */
static struct omap_bo *bo_y[6];   
static struct omap_bo *bo_uv[6];

/**
 *****************************************************************************
 * @brief:  free allocated input buffers
 *
 * @param:  coplanar  int
 *
 *****************************************************************************
*/
void release_input_buffers(int coplanar)
{
        int i;

        for (i = 0; i < NUMBUF; i++) {
                omap_bo_del(bo_y[i]);
                if (coplanar)
                        omap_bo_del(bo_uv[i]);
        }
}

/**
 *****************************************************************************
 * @brief:  read image from file & fill it in input buffer
 *
 * @param:  str  char pointer
 * @param:  fd   int
 * @param:  addr void pointer
 * @param:  size int
 *
 * @return: >0 	 number of bytes read 
 * @return: 0    end of file 
 *****************************************************************************
*/
int do_read (const char *str, int fd, void *addr, int size) {
	int nbytes = size, ret = 0, val;
	do {
		nbytes = size - ret;
		addr = addr + ret;
		if (nbytes == 0) {
			break;
		}
		ret = read(fd, addr, nbytes);
	} while(ret > 0);

	if (ret < 0) {
		val = errno;
		printf ("Read failed %s: %d %s\n", str, ret, strerror(val));
		exit (1);
	} else {
		dprintf ("Total bytes read %s = %d\n", str, size);
	}

	return ret;
}

/**
 *****************************************************************************
 * @brief:  allocates input buffer for vpe process
 *
 * @param:  vpe      struct vpe pointer
 * @param:  base     void pointer
 * @param:  base_uv  void pointer
 *
 * @return: 0 on success 
 *****************************************************************************
*/
int allocate_input_buffers(struct vpe *vpe, void *base[], void *base_uv[])
{
	int i;
        uint32_t bo_flags = OMAP_BO_WC;

         for (i = 0; i < NUMBUF; i++) {
                bo_y[i] = omap_bo_new(vpe->disp->dev, vpe->src.size,
				      bo_flags);

		/** Get DMABUF fd for corresponding buffer object */
                vpe->input_buf_dmafd[i] = omap_bo_dmabuf(bo_y[i]);

                /** Get virtual address for Y buffers */
                base[i] = omap_bo_map(bo_y[i]);
		dprintf("base[i] = 0x%x\n", i, base[i]);

                if(vpe->src.coplanar) {
                        bo_uv[i] = omap_bo_new(vpe->disp->dev, vpe->src.size_uv,
					       bo_flags);

			/** Get DMABUF fd for corresponding buffer object */
                        vpe->input_buf_dmafd_uv[i] = omap_bo_dmabuf(bo_uv[i]);

                	/** Get virtual address for UV buffers */
                        base_uv[i] = omap_bo_map(bo_uv[i]);
			dprintf("base_uv[i] = 0x%x\n", i, base_uv[i]);
                }

        }

        return 0;
}

int main (int argc, char *argv[])
{
	int	i, index = -1;

	void	*srcBuffers[NUMBUF];
	void	*srcBuffers_uv[NUMBUF];
	int	num_frames = 0;

	struct	vpe *vpe;

	/* No. of arguments is 16 or more. For kmscube based display it is 17 or more */
	if (argc < 16) {
		printf (
		"USAGE : <SRCfilename> <SRCWidth> <SRCHeight> <SRCFormat> "
			"<DSTWidth> <DSTHeight> <DSTformat> "
			"<CropTop> <CropLeft> <CropWidth> <CropHeight> "
			"<interlace> <translen> -s <connector_id>:<mode>\n"
			"Note:\n<interlace>\n1 - Deinterlace Alternate frame type\n"
			"2 - Deinterlace Sequence TB frame type\n");

		return 1;
	}

	/** Open the device */
	vpe = vpe_open();

	/** Open input file in read only mode */
	fin		= open (argv[1], O_RDONLY);
	vpe->src.width	= atoi (argv[2]);
	vpe->src.height	= atoi (argv[3]);
	describeFormat (argv[4], &vpe->src);

	vpe->dst.width	= atoi (argv[5]);
	vpe->dst.height = atoi (argv[6]);
	describeFormat (argv[7], &vpe->dst);

	vpe->crop.c.top = atoi (argv[8]);
	vpe->crop.c.left = atoi (argv[9]);
	vpe->crop.c.width = atoi (argv[10]);
	vpe->crop.c.height = atoi (argv[11]);
	vpe->crop.type = V4L2_BUF_TYPE_VIDEO_OUTPUT_MPLANE;

	vpe->deint = atoi (argv[12]);
	vpe->translen = atoi (argv[13]);

	dprintf ("Input  @ %d = %d x %d , %d\nOutput = %d x %d , %d\n",
		fin,  vpe->src.width, vpe->src.height, vpe->src.fourcc,
		vpe->dst.width, vpe->dst.height, vpe->dst.fourcc);

	if (fin  < 0 || vpe->src.height < 0 || vpe->src.width < 0 || \
	    vpe->src.fourcc < 0 || vpe->dst.height < 0 || \
	    vpe->dst.width < 0 || vpe->dst.fourcc < 0 || \
	    vpe->crop.c.top < 0 || vpe->crop.c.left < 0) {
	    pexit("Invalid parameters\n");
	}

        vpe->disp = disp_open(argc, argv);
	if(!vpe->disp)
		pexit("Can't open display\n");

	dprintf("display open success!!!\n");

	vpe_input_init(vpe);

	allocate_input_buffers(vpe, srcBuffers, srcBuffers_uv);

	vpe_output_init(vpe);

	for (i = 0; i < NUMBUF; i++)
		vpe_output_qbuf(vpe, i);

	dprintf ("Input  Buffers = %d @ size %d\nOutput Buffers = %d @ size %d\n",
		vpe->src.numbuf, vpe->src.size, vpe->dst.numbuf,
		vpe->dst.size);
	
	/*************************************
		Driver is ready Now
	*************************************/

	/** Read into the OUTPUT buffers from fin file */

	switch (vpe->deint) {
	case 1:
		vpe->field = V4L2_FIELD_TOP;
		break;
	case 2:
		vpe->field = V4L2_FIELD_SEQ_TB;
		break;
	case 0:
	default:
		vpe->field = V4L2_FIELD_ANY;
		break;
	}

	for (i = 0; i < NUMBUF; i++) {
		do_read("Y plane", fin, srcBuffers[i], vpe->src.size);
		if (vpe->src.coplanar)
			do_read("UV plane", fin, srcBuffers_uv[i], 
				 vpe->src.size_uv);

		vpe_input_qbuf(vpe, i);

		if (vpe->deint == 1) {
			if (vpe->field == V4L2_FIELD_TOP)
				vpe->field = V4L2_FIELD_BOTTOM;
			else
				vpe->field = V4L2_FIELD_TOP;
		}
	}

	/*************************************
		Data is ready Now
	*************************************/

	stream_ON(vpe->fd, V4L2_BUF_TYPE_VIDEO_OUTPUT_MPLANE);
	stream_ON(vpe->fd, V4L2_BUF_TYPE_VIDEO_CAPTURE_MPLANE);

	while (1) {

		index = vpe_input_dqbuf(vpe);

		if (do_read ("Y plane", fin, srcBuffers[index], 
			      vpe->src.size) <= 0)
			break;
		if (vpe->src.coplanar)
			if(do_read ("UV plane", fin, srcBuffers_uv[index],
				     vpe->src.size_uv) <= 0)
				break;

		vpe_input_qbuf(vpe, index);
		if (vpe->deint == 1) {
			if (vpe->field == V4L2_FIELD_TOP)
				vpe->field = V4L2_FIELD_BOTTOM;
			else
				vpe->field = V4L2_FIELD_TOP;
		}

		index = vpe_output_dqbuf(vpe);
		display_buffer(vpe, index);
		vpe_output_qbuf(vpe, index);
		num_frames++;

		printf("frames completed %d\n\n", num_frames);

		if (vpe->deint == 2) {
			index = vpe_output_dqbuf(vpe);
			display_buffer(vpe, index);
			vpe_output_qbuf(vpe, index);
			num_frames++;

			printf("frames completed %d\n\n", num_frames);
		}
	}

	printf("Done!!!\n");

	/** Driver cleanup */
	stream_OFF(vpe->fd, V4L2_BUF_TYPE_VIDEO_OUTPUT_MPLANE);
	stream_OFF(vpe->fd, V4L2_BUF_TYPE_VIDEO_CAPTURE_MPLANE);

	release_input_buffers(vpe->src.coplanar);

	disp_close(vpe->disp);
	vpe_close(vpe);
	close(fin);

	return 0;
}

