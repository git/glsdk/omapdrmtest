/*
 * Copyright (C) 2012 Texas Instruments
 * Author: Ramprasad(x0038811@ti.com)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include <libdce.h>
#include <xf86drm.h>
#include <omap_drm.h>
#include <omap_drmif.h>
#include "util.h"

#define MAX_IO_BUFS 16

/* omap drm device handle */
struct omap_device *dev = NULL;

extern void *dsp_dce_alloc(int sz);
extern void dsp_dce_free(void *ptr);

struct decoder {
	Engine_Handle engine;
	VIDDEC2_Handle codec;
	VIDDEC2_Params *params;
	VIDDEC2_DynamicParams *dynParams;
	VIDDEC2_Status *status;
	XDM1_BufDesc *inBufs;
	XDM_BufDesc *outBufs;
	VIDDEC2_InArgs *inArgs;
	VIDDEC2_OutArgs *outArgs;
	char *input;
	char *output;
	struct omap_bo *input_bo;
	struct omap_bo *output_bo;
    int input_sz, output_sz;
    XDAS_Int8 **outBuffer;
    XDAS_Int32 *outBufSizes;
};

static void
decoder_close(struct decoder *decoder)
{
    if(!decoder) goto bailout;
	if (decoder->params)         dsp_dce_free(decoder->params);
	if (decoder->dynParams)      dsp_dce_free(decoder->dynParams);
	if (decoder->status)         dsp_dce_free(decoder->status);
	if (decoder->inBufs)
    {
        dsp_dce_buf_unlock(1, &(decoder->inBufs->descs[0].buf));
        close(decoder->inBufs->descs[0].buf);
        dsp_dce_free(decoder->inBufs);
    }
	if (decoder->outBufs)        dsp_dce_free(decoder->outBufs);
	if (decoder->inArgs)         dsp_dce_free(decoder->inArgs);
	if (decoder->outArgs)        dsp_dce_free(decoder->outArgs);
	if (decoder->input_bo)       omap_bo_del(decoder->input_bo);
	if (decoder->output_bo)      omap_bo_del(decoder->output_bo);

	if (decoder->outBuffer)
    {
        close(decoder->outBuffer[0]);
        dsp_dce_free(decoder->outBuffer);
    }
	if (decoder->outBufSizes)    dsp_dce_free(decoder->outBufSizes);
	if (decoder->codec)          VIDDEC2_delete(decoder->codec);
	if (decoder->engine)         Engine_close(decoder->engine);

	free(decoder);
bailout:
	if (dev)		             dce_deinit(dev);
}

static struct decoder *
decoder_open(int pattern)
{
	struct decoder *decoder;
	int i;
	int width = 176, height = 144;
	Engine_Error ec;
	XDAS_Int32 err;

	decoder = calloc(1, sizeof(*decoder));
	if (!decoder)
		return NULL;

	MSG("%p: Opening Engine..", decoder);
	dev = dce_init();
	if(dev == NULL) {
		ERROR("%p: dce init failed", dev);
		goto fail;
	}
	decoder->engine = Engine_open("dsp_vidsvr", NULL, &ec);
	if (!decoder->engine) {
		ERROR("%p: could not open engine", decoder);
		goto fail;
	}
	decoder->input_sz = width * height;
	decoder->input_bo = omap_bo_new(dev, decoder->input_sz, OMAP_BO_WC);
	decoder->input = omap_bo_map(decoder->input_bo);

	decoder->output_sz = width * height;
	decoder->output_bo = omap_bo_new(dev, decoder->output_sz, OMAP_BO_WC);
	decoder->output = omap_bo_map(decoder->output_bo);

	decoder->params = dsp_dce_alloc(sizeof(IVIDDEC2_Params));
	decoder->params->size = sizeof(IVIDDEC2_Params);

	decoder->codec = VIDDEC2_create(decoder->engine,
                                        "dsp_universalCopy", decoder->params);

	if (!decoder->codec) {
		ERROR("%p: could not create codec", decoder);
		goto fail;
	}
    MSG("Created dsp_universalCopy \n");
	decoder->dynParams = dsp_dce_alloc(sizeof(IVIDDEC2_DynamicParams));
	decoder->dynParams->size = sizeof(IVIDDEC2_DynamicParams);

	decoder->status = dsp_dce_alloc(sizeof(IVIDDEC2_Status));
	decoder->status->size = sizeof(IVIDDEC2_Status);

	err = VIDDEC2_control(decoder->codec, XDM_SETPARAMS,
			decoder->dynParams, decoder->status);
	if (err) {
		ERROR("%p: fail: %d", decoder, err);
		goto fail;
	}
	/* not entirely sure why we need to call this here.. just copying omx.. */
	err = VIDDEC2_control(decoder->codec, XDM_GETBUFINFO,
			decoder->dynParams, decoder->status);
	if (err) {
		ERROR("%p: fail: %d", decoder, err);
		goto fail;
	}
    decoder->outBuffer = dsp_dce_alloc(sizeof(XDAS_Int8*) * MAX_IO_BUFS);
    decoder->outBufSizes = dsp_dce_alloc(sizeof(XDAS_Int32)* MAX_IO_BUFS);
    decoder->inBufs = dsp_dce_alloc(sizeof(XDM1_BufDesc));
    decoder->inBufs->numBufs = 1;
    decoder->inBufs->descs[0].buf =	(XDAS_Int8 *)omap_bo_dmabuf((struct omap_bo*) decoder->input_bo);
    decoder->inBufs->descs[0].bufSize = width * height;
    dsp_dce_buf_lock(1, &(decoder->inBufs->descs[0].buf));

    decoder->outBuffer[0] = (XDAS_Int8*)omap_bo_dmabuf((struct omap_bo*) decoder->output_bo);
    decoder->outBuffer[1] = decoder->outBuffer[0];
    decoder->outBufSizes[0] = width * height;
    decoder->outBufSizes[1] = decoder->outBufSizes[0];
    decoder->outBufs = dsp_dce_alloc(sizeof(XDM_BufDesc));
	decoder->outBufs->numBufs = 2;
	decoder->outBufs->bufs = (XDAS_Int8**)decoder->outBuffer;
    decoder->outBufs->bufSizes = &decoder->outBufSizes[0];
    dsp_dce_buf_lock(1, &decoder->outBuffer[0]);

	decoder->inArgs = dsp_dce_alloc(sizeof(IVIDDEC2_InArgs));
	decoder->inArgs->size = sizeof(IVIDDEC2_InArgs);

	decoder->outArgs = dsp_dce_alloc(sizeof(IVIDDEC2_OutArgs));
	decoder->outArgs->size = sizeof(IVIDDEC2_OutArgs);

    // Fill input buffer with a pattern for testing */
    MSG("Fill input buffer with pattern %d\n", pattern);
    for(i = 0; i < decoder->input_sz ; i++)
        decoder->input[i] = pattern;

	return decoder;


usage:
	MSG("Fail\n");
fail:
	if (decoder)
	   decoder_close(decoder);
	return NULL;
}

static int
decoder_process(struct decoder *decoder)
{
    XDM1_BufDesc *inBufs = decoder->inBufs;
    XDM_BufDesc *outBufs = decoder->outBufs;
    VIDDEC2_InArgs *inArgs = decoder->inArgs;
    VIDDEC2_OutArgs *outArgs = decoder->outArgs;
    XDAS_Int32 err = XDM_EOK, j,success = XDAS_TRUE;
    char *src,*dst;

    err = VIDDEC2_process(decoder->codec, inBufs, outBufs, inArgs, outArgs);
    if (err) {
       ERROR("%p: process returned error: %d", decoder, err);
       return -1;
      }

    dsp_dce_buf_unlock(1, &decoder->outBuffer[0]);

    MSG("Verifing the UniversalCopy algorithm\n");

    src = (char*)decoder->input;
    dst = (char*)decoder->output;
    for(j= 0 ; j < decoder->input_sz ; j++){
       if(*src != *dst){
            MSG("copycodectest failed at %d, 0x%x, 0x%x\n", j, *src, *dst);
            success = XDAS_FALSE;
            break;
          }
          src++;
          dst++;
     }
      if(success ==XDAS_TRUE)
           MSG("copycodectest executed successfully\n");

	return 0;
}

int
main(int argc, char **argv)
{
    struct decoder *decoder = NULL;
    int pattern;
    if(argc != 2){
       MSG("Usage: copycodectest pattern(0 to 255) \t");
       MSG("Example: copycodectest 100\n");
       exit(0);
    }
    pattern = atoi(argv[1]);
    decoder = decoder_open(pattern);

    if(decoder)
       decoder_process(decoder);

    if(decoder)
       decoder_close(decoder);

    return 0;
}
